import React, { Component, PropTypes } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import StyleVars from 'NOC/src/styles/StyleVars';

const isAndroid = Platform.OS === 'android';

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    overflow: 'hidden',
  },
  buttonText: {
    color: StyleVars.Colors.titleText,
    fontFamily: StyleVars.Fonts.general,
    fontSize: 12,
    fontWeight: '600'
  }
});

class Button extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    style: View.propTypes.style,
    textStyle: Text.propTypes.style,
    activeOpacity: PropTypes.number,
    enabled: PropTypes.bool,
    children: PropTypes.string,
    limitString: PropTypes.number,
    icon: PropTypes.string,
    color: PropTypes.string,
    backgroundColor: PropTypes.string,
  }
  static defaultProps = {
    onPress: () => {},
    style: {},
    textStyle: {},
    activeOpacity: 0.8,
    enabled: true,
    limitString: 30,
    color: '#FFF',
    backgroundColor: StyleVars.Colors.primary,
  }
  render() {
    const icon = this.props.icon ? <Icon
      size={30}
      name={this.props.icon} 
      color={this.props.color} /> : null;
    const text = this.props.children != undefined ? <Text 
      style={[{padding: 10}, styles.buttonText]}
    >
      { this.props.children.substr(0, this.props.limitString) }
    </Text> : null;
    return (
      <TouchableOpacity
        activeOpacity={this.props.activeOpacity}
        onPress={() => this.onPress()}
        style={[styles.button, {backgroundColor: this.props.backgroundColor}, this.props.style]}
      >
        { this.props.image }
        { icon }
        { text }
      </TouchableOpacity>
    );
  }

  onPress() {
    if (this.props.enabled) {
      this.props.onPress();
    }
  }
}

export default Button;

export const BackButton = props => <Button 
  icon='keyboard-arrow-left' 
  {...props} 
  style={
    isAndroid ? SharedStyles.button : SharedStyles.buttonIOS
  }
  onPress={() => props.onPress('BACK')}
/>;

export const MoreButton = props => <Button 
  icon='settings' 
  {...props} 
  style={
    isAndroid ? SharedStyles.button : SharedStyles.buttonIOS
  }
  onPress={() => props.onPress('MORE')}
/>;

export const MenuButton = props => <Button 
  icon='menu' 
  {...props}
  style={
    isAndroid ? SharedStyles.button : SharedStyles.buttonIOS
  }
  onPress={() => props.onPress('MENU')} 
/>;

export const SearchButton = props => <Button
  icon='search'
  {...props}
  style={
    isAndroid ? SharedStyles.button : SharedStyles.buttonIOS
  }
  onPress={() => props.onPress('SEARCH')} 
/>;

export const SearchAndMoreButtons = props => <View 
  style={{flexDirection: 'row'}}
>
  <SearchButton {...props} />
  <MoreButton {...props} />
</View>;
