import React, { Component, PropTypes } from 'react';
import {
  Easing,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Const from 'NOC/src/lib/Const';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import StyleVars from 'NOC/src/styles/StyleVars';

const styles = StyleSheet.create({
  status: {
    padding: 5,
    backgroundColor: StyleVars.Colors.primary
  }
});

export default class StatsView extends Component {
  static propTypes = {
    text: PropTypes.string,
    severity: PropTypes.bool,
    tech: PropTypes.bool,
    priority: PropTypes.bool
  }
  static defaultProps = {
    text: '',
    severity: false,
    tech: false,
    priority: false
  }
  render() {
    const { data } = this.props;
    const icon2 = data.icon2 ? `2G (${data.icon2})` : null;
    const icon25 = data.icon25 ? `2.5G (${data.icon25})` : null;
    const icon3 = data.icon3 ? `3G (${data.icon3})` : null;
    const icon4 = data.icon4 ? `4G (${data.icon4})` : null;

    const priority0 = data.priority0 ? `0 (${data.priority0})` : null;
    const priority1 = data.priority1 ? `1 (${data.priority1})` : null;
    const priority2 = data.priority2 ? `2 (${data.priority2})` : null;
    const priority3 = data.priority3 ? `3 (${data.priority3})` : null;

    const critical = data.Critical ? `Critical (${data.Critical})` : null;
    const major = data.Major ? `Major (${data.Major})` : null;

    const tech = this.props.tech ? <View>
      <View style={SharedStyles.cellRow}>
        <Text style={[SharedStyles.labelText, {color: 'white'}]}>
          { Const.TECHS }:
        </Text>
        <Text style={[SharedStyles.text, {color: 'white'}]}>
          { icon2 } { icon25 } { icon3 } { icon4 }
        </Text>
      </View>
    </View> : null;
    const priority = this.props.priority ? <View>
      <View style={SharedStyles.cellRow}>
        <Text style={[SharedStyles.labelText, {color: 'white'}]}>
          { Const.PRIORITIES }:
        </Text>
        <Text style={[SharedStyles.text, {color: 'white'}]}>
          { priority0 } { priority1 } { priority2 } { priority3 }
        </Text>
      </View>
    </View> : null;
    const severity = this.props.severity ? <View>
      <View style={SharedStyles.cellRow}>
        <Text style={[SharedStyles.labelText, {color: 'white'}]}>
          { Const.SEVERITY }:
        </Text>
        <Text style={[SharedStyles.text, {color: 'white'}]}>
          { critical } { major }
        </Text>
      </View>
    </View> : null;
    return (
      <View 
        elevation={3} 
        style={styles.status}>
        { tech }
        { priority }
        { severity }
      </View>
    );
  }
}
