import React, { Component } from 'react';
import { Marker } from 'react-native-maps';
import Const from 'NOC/src/lib/Const';

export default class SiteMarker extends Component {
  getMarker(site) {
    let marker = null;
    let path = `icon${site[2] || ''}${site[25] || ''}${site[3] || ''}${site[4] || ''}.png`;
    // react native still doesn't support variables in require method 'wtf'
    // facebook says that react needs to analyze assets before running
    if (this.props.size === Const.LARGE) {
      if (path == 'icon2.png') marker = require('NOC/img/markers-large/icon2.png');
      else if (path == 'icon225.png') marker = require('NOC/img/markers-large/icon225.png');
      else if (path == 'icon2253.png') marker = require('NOC/img/markers-large/icon2253.png');
      else if (path == 'icon22534.png') marker = require('NOC/img/markers-large/icon22534.png');
      else if (path == 'icon22534alert.png') marker = require('NOC/img/markers-large/icon22534alert.png');
      else if (path == 'icon2253alert.png') marker = require('NOC/img/markers-large/icon2253alert.png');
      else if (path == 'icon2254.png') marker = require('NOC/img/markers-large/icon2254.png');
      else if (path == 'icon2254alert.png') marker = require('NOC/img/markers-large/icon2254alert.png');
      else if (path == 'icon225alert.png') marker = require('NOC/img/markers-large/icon225alert.png');
      else if (path == 'icon23.png') marker = require('NOC/img/markers-large/icon23.png');
      else if (path == 'icon234.png') marker = require('NOC/img/markers-large/icon234.png');
      else if (path == 'icon234alert.png') marker = require('NOC/img/markers-large/icon234alert.png');
      else if (path == 'icon23alert.png') marker = require('NOC/img/markers-large/icon23alert.png');
      else if (path == 'icon24.png') marker = require('NOC/img/markers-large/icon24.png');
      else if (path == 'icon24alert.png') marker = require('NOC/img/markers-large/icon24alert.png');
      else if (path == 'icon25.png') marker = require('NOC/img/markers-large/icon25.png');
      else if (path == 'icon253.png') marker = require('NOC/img/markers-large/icon253.png');
      else if (path == 'icon2534.png') marker = require('NOC/img/markers-large/icon2534.png');
      else if (path == 'icon2534alert.png') marker = require('NOC/img/markers-large/icon2534alert.png');
      else if (path == 'icon253alert.png') marker = require('NOC/img/markers-large/icon253alert.png');
      else if (path == 'icon254.png') marker = require('NOC/img/markers-large/icon254.png');
      else if (path == 'icon254alert.png') marker = require('NOC/img/markers-large/icon254alert.png');
      else if (path == 'icon25alert.png') marker = require('NOC/img/markers-large/icon25alert.png');
      else if (path == 'icon2alert.png') marker = require('NOC/img/markers-large/icon2alert.png');
      else if (path == 'icon3.png') marker = require('NOC/img/markers-large/icon3.png');
      else if (path == 'icon34.png') marker = require('NOC/img/markers-large/icon34.png');
      else if (path == 'icon34alert.png') marker = require('NOC/img/markers-large/icon34alert.png');
      else if (path == 'icon3alert.png') marker = require('NOC/img/markers-large/icon3alert.png');
      else if (path == 'icon4.png') marker = require('NOC/img/markers-large/icon4.png');
      else if (path == 'icon4alert.png') marker = require('NOC/img/markers-large/icon4alert.png');
    }
    else if (this.props.size === Const.SMALL) {
      if (path == 'icon2.png') marker = require('NOC/img/markers-small/icon2.png');
      else if (path == 'icon225.png') marker = require('NOC/img/markers-small/icon225.png');
      else if (path == 'icon2253.png') marker = require('NOC/img/markers-small/icon2253.png');
      else if (path == 'icon22534.png') marker = require('NOC/img/markers-small/icon22534.png');
      else if (path == 'icon22534alert.png') marker = require('NOC/img/markers-small/icon22534alert.png');
      else if (path == 'icon2253alert.png') marker = require('NOC/img/markers-small/icon2253alert.png');
      else if (path == 'icon2254.png') marker = require('NOC/img/markers-small/icon2254.png');
      else if (path == 'icon2254alert.png') marker = require('NOC/img/markers-small/icon2254alert.png');
      else if (path == 'icon225alert.png') marker = require('NOC/img/markers-small/icon225alert.png');
      else if (path == 'icon23.png') marker = require('NOC/img/markers-small/icon23.png');
      else if (path == 'icon234.png') marker = require('NOC/img/markers-small/icon234.png');
      else if (path == 'icon234alert.png') marker = require('NOC/img/markers-small/icon234alert.png');
      else if (path == 'icon23alert.png') marker = require('NOC/img/markers-small/icon23alert.png');
      else if (path == 'icon24.png') marker = require('NOC/img/markers-small/icon24.png');
      else if (path == 'icon24alert.png') marker = require('NOC/img/markers-small/icon24alert.png');
      else if (path == 'icon25.png') marker = require('NOC/img/markers-small/icon25.png');
      else if (path == 'icon253.png') marker = require('NOC/img/markers-small/icon253.png');
      else if (path == 'icon2534.png') marker = require('NOC/img/markers-small/icon2534.png');
      else if (path == 'icon2534alert.png') marker = require('NOC/img/markers-small/icon2534alert.png');
      else if (path == 'icon253alert.png') marker = require('NOC/img/markers-small/icon253alert.png');
      else if (path == 'icon254.png') marker = require('NOC/img/markers-small/icon254.png');
      else if (path == 'icon254alert.png') marker = require('NOC/img/markers-small/icon254alert.png');
      else if (path == 'icon25alert.png') marker = require('NOC/img/markers-small/icon25alert.png');
      else if (path == 'icon2alert.png') marker = require('NOC/img/markers-small/icon2alert.png');
      else if (path == 'icon3.png') marker = require('NOC/img/markers-small/icon3.png');
      else if (path == 'icon34.png') marker = require('NOC/img/markers-small/icon34.png');
      else if (path == 'icon34alert.png') marker = require('NOC/img/markers-small/icon34alert.png');
      else if (path == 'icon3alert.png') marker = require('NOC/img/markers-small/icon3alert.png');
      else if (path == 'icon4.png') marker = require('NOC/img/markers-small/icon4.png');
      else if (path == 'icon4alert.png') marker = require('NOC/img/markers-small/icon4alert.png');
    }
    else {
      if (path == 'icon2.png') marker = require('NOC/img/markers-medium/icon2.png');
      else if (path == 'icon225.png') marker = require('NOC/img/markers-medium/icon225.png');
      else if (path == 'icon2253.png') marker = require('NOC/img/markers-medium/icon2253.png');
      else if (path == 'icon22534.png') marker = require('NOC/img/markers-medium/icon22534.png');
      else if (path == 'icon22534alert.png') marker = require('NOC/img/markers-medium/icon22534alert.png');
      else if (path == 'icon2253alert.png') marker = require('NOC/img/markers-medium/icon2253alert.png');
      else if (path == 'icon2254.png') marker = require('NOC/img/markers-medium/icon2254.png');
      else if (path == 'icon2254alert.png') marker = require('NOC/img/markers-medium/icon2254alert.png');
      else if (path == 'icon225alert.png') marker = require('NOC/img/markers-medium/icon225alert.png');
      else if (path == 'icon23.png') marker = require('NOC/img/markers-medium/icon23.png');
      else if (path == 'icon234.png') marker = require('NOC/img/markers-medium/icon234.png');
      else if (path == 'icon234alert.png') marker = require('NOC/img/markers-medium/icon234alert.png');
      else if (path == 'icon23alert.png') marker = require('NOC/img/markers-medium/icon23alert.png');
      else if (path == 'icon24.png') marker = require('NOC/img/markers-medium/icon24.png');
      else if (path == 'icon24alert.png') marker = require('NOC/img/markers-medium/icon24alert.png');
      else if (path == 'icon25.png') marker = require('NOC/img/markers-medium/icon25.png');
      else if (path == 'icon253.png') marker = require('NOC/img/markers-medium/icon253.png');
      else if (path == 'icon2534.png') marker = require('NOC/img/markers-medium/icon2534.png');
      else if (path == 'icon2534alert.png') marker = require('NOC/img/markers-medium/icon2534alert.png');
      else if (path == 'icon253alert.png') marker = require('NOC/img/markers-medium/icon253alert.png');
      else if (path == 'icon254.png') marker = require('NOC/img/markers-medium/icon254.png');
      else if (path == 'icon254alert.png') marker = require('NOC/img/markers-medium/icon254alert.png');
      else if (path == 'icon25alert.png') marker = require('NOC/img/markers-medium/icon25alert.png');
      else if (path == 'icon2alert.png') marker = require('NOC/img/markers-medium/icon2alert.png');
      else if (path == 'icon3.png') marker = require('NOC/img/markers-medium/icon3.png');
      else if (path == 'icon34.png') marker = require('NOC/img/markers-medium/icon34.png');
      else if (path == 'icon34alert.png') marker = require('NOC/img/markers-medium/icon34alert.png');
      else if (path == 'icon3alert.png') marker = require('NOC/img/markers-medium/icon3alert.png');
      else if (path == 'icon4.png') marker = require('NOC/img/markers-medium/icon4.png');
      else if (path == 'icon4alert.png') marker = require('NOC/img/markers-medium/icon4alert.png');
    }
    return marker;
  }
  render() {
    const { site } = this.props;
    // If they solve require in react native then this line
    // bellow delete all line abow
    // const path = `NOC/img/markers-medium/icon${`${site[2] || ''}${site[25] || ''}${site[3] || ''}${site[4] || ''}.png`}`;
    return (
      <Marker
        onPress={this.props.onPress}
        key={site.code}
        title={site.code}
        image={this.getMarker(site)}
        coordinate={{
          longitude: site.lon || site.lon !== 0 ? site.lon : 20.4664,
          latitude: site.lat || site.lat !== 0 ? site.lat : 44.8025,
        }}
      />
    );
  }
}