import React, { Component, PropTypes } from 'react';
import {
  UIManager,
  LayoutAnimation,
  TouchableWithoutFeedback,
  Image,
  View,
  Text,
} from 'react-native';
import Routes from 'NOC/src/Routes';
import Button from 'NOC/src/Views/Button';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import dateFormat from 'dateformat';
import Const from 'NOC/src/lib/Const';
import CustomLayoutAnimation from 'NOC/src/lib/CustomLayoutAnimation';
import { connect } from 'react-redux';
import { searchMarkers } from 'NOC/src/actions';
import { bindActionCreators } from 'redux';

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class FailureCell extends Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    failure: PropTypes.object.isRequired,
  }
  static defaultProps = {
    onPress: () => {},
  }
  state = {
    expanded: false,
  }
  componentWillUpdate() {
    LayoutAnimation.configureNext(CustomLayoutAnimation);
  }
  _moreInfo(station) {
    let moreInfoRoute = Routes.moreInfo();
    moreInfoRoute.passProps = { station };
    this.props.navigator.push(moreInfoRoute);
  }
  _positonOnMap(station) {
    this.props.searchMarkers(station.code);
    this.props.navigator.push(Routes.map());
  }
  render() {
    const { failure } = this.props;
    let color = 'white';
    if (failure.priority == 0) color = '#f44336';
    if (failure.priority == 1) color = '#FF9800';
    if (failure.priority == 2) color = '#FFEB3B';
    if (failure.priority == 3) color = '#4CAF50';
    const currentDate = new Date();
    const originalTime = new Date(failure.originalTime);
    const lastOccurrence = failure.lastOccurrence ? new Date(failure.lastOccurrence) : null;
    const duration = failure.lastOccurrence ? new Date(lastOccurrence - originalTime) : new Date(currentDate - originalTime);
    failure.formatedDurationTime = formatDurationTime(duration);
    failure.formatedOriginalTime = dateFormat(originalTime, 'HH:MM:ss - dd/mm/yyyy');
    if (lastOccurrence) failure.formatedLastOccurrence = dateFormat(lastOccurrence, 'HH:MM:ss - dd/mm/yyyy');
    let icon = null;
    let marker = null;
    if (failure.icon == 2) {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/tower2.png`)}
      />;
      marker = <Button
        backgroundColor='white'
        image={<Image 
          style={SharedStyles.icon} 
          source={require(`NOC/img/markers-large/icon2.png`)}
        />}
        onPress={() => this._positonOnMap(this.props.failure)}
      />
    }
    else if (failure.icon == 25) {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/tower25.png`)}
      />;
      marker = <Button
        backgroundColor='white'
        image={<Image 
          style={SharedStyles.icon} 
          source={require(`NOC/img/markers-large/icon25.png`)}
        />}
        onPress={() => this._positonOnMap(this.props.failure)}
      />
    }
    else if (failure.icon == 3) {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/tower3.png`)}
      />;
      marker = <Button
        backgroundColor='white'
        image={<Image 
          style={SharedStyles.icon} 
          source={require(`NOC/img/markers-large/icon3.png`)}
        />}
        onPress={() => this._positonOnMap(this.props.failure)}
      />;
    }
    else if (failure.icon == 4) {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/tower4.png`)}
      />;
      marker = <Button
        backgroundColor='white'
        image={<Image 
          style={SharedStyles.icon} 
          source={require(`NOC/img/markers-large/icon4.png`)}
        />}
        onPress={() => this._positonOnMap(this.props.failure)}
      />;
    }
    else {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require('NOC/img/tower.png')}
      />;
      marker = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/markers-large/noico.png`)}
      />;
    }
    const codeName = failure.code ? <TouchableWithoutFeedback 
      onPress={() => this.setState({expanded: !this.state.expanded})}
    >
      <View 
        style={[SharedStyles.cellRow, {margin: 5}]}
      >
        { icon }
        <Button 
          style={{margin: 3}} 
          onPress={() => this.setState({expanded: !this.state.expanded})}
          color='#263238'
          backgroundColor='white'
          icon={this.state.expanded ? 'expand-less' : 'expand-more'}
        />
        <Text 
          style={[SharedStyles.headingText, {flex: 1, marginTop: 10}]}
        >
          {failure.code} - {failure.name}
        </Text>
        { marker }
      </View>
    </TouchableWithoutFeedback> : null;
    const content = Object.keys(Const.FAILURES_LABELS).map((key, i) => {
      let data = failure[Const.FAILURES_LABELS[key]];
      if (data || data == 0) {
        return (
          <View key={i}>
            { 
              i !== 0 ? <View 
              style={[SharedStyles.rowSeparator, {marginVertical: 5}]} 
              /> : null
            }
            <View 
              style={SharedStyles.cellRow}
            >
              <Text 
                style={SharedStyles.labelText}
              >
                {key}: 
              </Text>   
              <Text 
                style={SharedStyles.text}>
                {data}
              </Text>
            </View>
          </View>
        );
      }
    });
    return (
      <View 
        style={SharedStyles.cellRow}
      >
        <View 
          style={[SharedStyles.block, {backgroundColor: color}]}
        />
          <View 
            style={SharedStyles.container}
          >
            { codeName }
            { 
              this.state.expanded ? <View>
                <View 
                  style={{margin: 15}}
                >
                  { content }
                </View>
                <Button 
                  onPress={() => this._moreInfo(this.props.failure)} 
                  style={{margin: 10}}
                >
                  {`Podaci stanice ${this.props.failure.code}`}
                </Button>
              </View> : null
            }
          </View>
        <View 
          style={[SharedStyles.block, {backgroundColor: color}]}
        />
      </View>
    );
  }
}

const formatDurationTime = millisecond => {
  var diffSeconds =   Math.floor(millisecond  / 1000 % 60);
  var diffMinutes =   Math.floor(millisecond  / (60 * 1000) % 60);
  var diffHours   =   Math.floor(millisecond  / (60 * 60 * 1000) % 24);
  var diffDays    =   Math.floor(millisecond  / (24 * 60 * 60 * 1000));

  diffDays    = diffDays ? pad(diffDays, 3) + 'd ' : '';
  diffHours   = diffHours ? pad(diffHours, 3) + 'h ' : '';
  diffMinutes = pad(diffMinutes, 3) + 'm';

  return diffDays + diffHours + diffMinutes;
}

const pad = (str, max) => {
  str = str.toString();
  return str.length < max ? pad(' ' + str, max) : str;
}

function mapDispatch(dispatch) {
  return bindActionCreators({searchMarkers}, dispatch);
}

export default connect(null, mapDispatch)(FailureCell);