import React, { Component, PropTypes } from 'react';
import {
  Alert,
  StyleSheet,
  Platform,
  ActivityIndicator,
  View,
  TextInput,
  Text,
  TouchableHighlight,
} from 'react-native';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import StyleVars from 'NOC/src/styles/StyleVars';
import Button from 'NOC/src/Views/Button';
import Const from 'NOC/src/lib/Const';
import Icon from 'react-native-vector-icons/MaterialIcons';

const styles = StyleSheet.create({
  delete: {
    margin: 15,
  }
});

export default class SearchBar extends Component {
  static propTypes = {
    isAndroid: PropTypes.bool,
    onSearch: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
  }
  static defaultProps = {
    isAndroid: Platform.OS === 'android',
    onSearch: () => {},
    placeholder: '',
  }
  clearText() {
    if (this.props.clearText) this.props.clearText();
    this.input.setNativeProps({text: ''});
  }
  render() {
    let key = 'K';
    if (this.props.settings.searchProperty === 'code') key = 'K';
    if (this.props.settings.searchProperty === 'nodeAlias') key = 'B';
    if (this.props.settings.searchProperty === 'name') key = 'I';
    if (this.props.settings.searchProperty === 'address') key = 'A';
    if (this.props.settings.searchProperty === 'municipalitie') key = 'O';
    return (
      <View 
        elevation={3} 
        style={SharedStyles.searchContainer}
      >
        <Button
          style={styles.delete}
          icon='arrow-back'
          onPress={this.clearText.bind(this)} 
        />
        <TextInput
          ref={ref => this.input = ref}
          placeholderTextColor={StyleVars.Colors.mediumBackground}
          autoCapitalization='none'
          autoCorrect={false}
          placeholder={this.props.placeholder}
          returnKeyType='search'
          enablesReturnKeyAutomatically={true}
          style={SharedStyles.searchInput}
          onChange={this.props.onSearch}
          onFocus={this.props.onFocus}
        />
        { 
          !this.props.isAndroid ? <ActivityIndicator 
            color='white' 
            animating={this.props.isLoading} 
            style={SharedStyles.searchSpinner}
          /> : null
        }
        <Button
          style={styles.delete}
          onPress={() => Alert.alert(
            Const.NOTIFICATION,
            Const.SEARCH_TYPES_MESSAGE,
          )}
        >
          { key }
        </Button>
      </View>
    )
  }
}