import React, { Component, PropTypes } from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Text,
} from 'react-native';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import dateFormat from 'dateformat';
import Const from 'NOC/src/lib/Const';

export default class AlertCell extends Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    alert: PropTypes.object.isRequired,
  }
  static defaultProps = {
    onPress: () => {},
  }
  render() {
    const { alert } = this.props;
    let color = '#66BB6A';
    if (!alert.state) {
      if (alert.originalSeverity == Const.CRITICAL) color = '#F44336';
      else if (alert.originalSeverity == Const.MAJOR) color = '#FF9800';
      else if (alert.originalSeverity == Const.MINOR) color = '#FFEE58';
      else if (alert.originalSeverity == Const.WARNING) color = '#4CAF50';
    }
    alert.formatedOriginalTime = dateFormat(new Date(alert.originalTime), 'HH:MM:ss - dd/mm/yyyy');
    const content = Object.keys(Const.ALERTS_LABELS).map((key, i) => {
      let data = alert[Const.ALERTS_LABELS[key]];
      if (data) return (
        <View key={i}>
          {
            i != 0 ? <View 
              style={[SharedStyles.rowSeparator, {marginBottom: 10}]} 
            /> : null
          }
          <View 
            style={SharedStyles.cellRow}
          >
            <Text 
              style={[SharedStyles.labelText, {color: 'white'}]}
            >
              {key}: 
            </Text>   
            <Text 
              style={[SharedStyles.text, {color: 'white'}]}
            >
              {data}
            </Text>
          </View>
        </View>
      );
    });
    return (
      <TouchableWithoutFeedback 
        onPress={this.props.onPress}
      >
        <View 
          style={{padding: 15, backgroundColor: color}}
        >
          { content }
        </View>
      </TouchableWithoutFeedback>
    );
  }
}