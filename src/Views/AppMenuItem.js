import React, { Component, PropTypes } from 'react';
import {
	StyleSheet,
	TouchableOpacity,
	View,
	Text
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import StyleVars from 'NOC/src/styles/StyleVars';

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row'
	},
	item: {
		flexDirection: 'row',
		marginTop: 15,
		marginBottom: 15,
		marginLeft: 20,
		color: 'white'
	},
	block: {
		width: 5
	}
});

export default class AppMenuItem extends Component {
	static propTypes = {
    onPress: PropTypes.func.isRequired,
		route: PropTypes.object.isRequired,
		activeOpacity: PropTypes.number,
		blockColor: PropTypes.string.isRequired,
		icon: PropTypes.string.isRequired,
  }
  static defaultProps = {
    onPress: () => {},
  }
	render() {
		return (
			<TouchableOpacity
				activeOpacity={this.props.activeOpacity}
				onPress={() => this.props.onPress(this.props.route)}
				style={styles.container}>
				<View 
					style={[styles.block, {backgroundColor: this.props.blockColor}]}
				/>
				<Icon 
					name={this.props.icon} size={20} color={StyleVars.Colors.secondary} style={styles.item}
				/>
				<Text 
					style={styles.item}>
					{this.props.route.title}
				</Text>
			</TouchableOpacity>
		);
	}
}