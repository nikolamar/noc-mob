import React, { Component, PropTypes } from 'react';
import {
	StyleSheet,
	View,
	Text,
	Image,
	TouchableOpacity
} from 'react-native';
import AppMenuItem from 'NOC/src/Views/AppMenuItem';
import Routes from 'NOC/src/Routes';
import StyleVars from 'NOC/src/styles/StyleVars';
import Const from 'NOC/src/lib/Const';
import Storage from 'NOC/src/lib/Storage';

const styles = StyleSheet.create({
	card: {
		flexDirection: 'row',
		height: 150,
		backgroundColor: StyleVars.Colors.darkBackground
	},
	cardImage: {
		marginLeft: 16,
		width: 150,
		height: 150
	},
	cardTitle: {
		position: 'absolute',
		left: 60, 
		top: 65,
		fontSize: 15,
		color: '#FFF'
	},
	menuContainer: {
		backgroundColor: StyleVars.Colors.darkBackground,
		flex: 1
	}
});

export default class AppMenu extends Component {
	static propTypes = {
    onPress: PropTypes.func.isRequired,
  }
  static defaultProps = {
    onPress: () => {},
  }
	state = {
		user: '',
		privilege: 0
	}
	async componentWillMount() {
		const privilege = await Storage.get(Const.PRIVILEGE);
		const username = await Storage.get(Const.USERNAME);
		if (username) {
			this.setState({username});
		}
		if (privilege) {
			this.setState({
				privilege: JSON.parse(privilege)
			});
		}
	}
	render() {
		return (
			<View 
				style={styles.menuContainer}
			>
				<TouchableOpacity
					onPress={() => this.props.onPress(Routes.about())}
				>
					<View 
						style={styles.card}
					>
						<Image
							style={styles.cardImage}
							source={require('NOC/img/logo.png')}
						/>
						<Text 
							style={styles.cardTitle}
						>
							{Const.APP_TITLE}
						</Text>
					</View>
				</TouchableOpacity>
				<AppMenuItem 
					onPress={this.props.onPress} 
					route={Routes.failures()} 
					icon='warning' 
					blockColor={StyleVars.Colors.primary}
				/>
				<AppMenuItem 
					onPress={this.props.onPress} 
					route={Routes.stations()} 
					icon='settings-input-antenna' 
					blockColor={StyleVars.Colors.primary}
				/>
				{
					this.state.privilege >= 3 ? <AppMenuItem 
						onPress={this.props.onPress} 
						route={Routes.alerts()} 
						icon='notifications' 
						blockColor={StyleVars.Colors.primary}/> : null
				}
				<AppMenuItem 
					onPress={this.props.onPress} 
					route={Routes.map()} 
					icon='map' 
					blockColor={StyleVars.Colors.primary}
				/>
				<AppMenuItem 
					onPress={this.props.onPress} 
					route={Routes.login()} 
					icon='exit-to-app' 
					blockColor={StyleVars.Colors.primary}
				/>
			</View>
		);
	}
}