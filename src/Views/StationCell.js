import React, { Component, PropTypes } from 'react';
import {
  UIManager,
  LayoutAnimation,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import Button from 'NOC/src/Views/Button';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import Const from 'NOC/src/lib/Const';
import Routes from 'NOC/src/Routes';
import { connect } from 'react-redux';
import { searchMarkers } from 'NOC/src/actions';
import { bindActionCreators } from 'redux';

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

let CustomLayoutAnimation = {
  duration: 200,
  create: {
    type: LayoutAnimation.Types.linear,
    property: LayoutAnimation.Properties.opacity,
  },
  update: {
    type: LayoutAnimation.Types.linear,
  },
};

class StationCell extends Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    station: PropTypes.object.isRequired,
  }
  static defaultProps = {
    onPress: () => {},
  }
  state = {
    expanded: false,
  }
  componentWillMount() {
    if (this.props.expanded)
      this.setState({expanded: true});
  }
  componentWillUpdate() {
    LayoutAnimation.configureNext(CustomLayoutAnimation);
  }
  _positonOnMap(station) {
    this.props.searchMarkers(station.code);
    this.props.navigator.push(Routes.map());
  }
  render() {
    const { station } = this.props;
    let color = 'white';
    if (station.priority == 0) color = '#f44336';
    if (station.priority == 1) color = '#FF9800';
    if (station.priority == 2) color = '#FFEB3B';
    if (station.priority == 3) color = '#4CAF50';
    let icon = null;
    let marker = null;
    if (station.icon == 2) {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/tower2.png`)}
      />;
      marker = <Button
        backgroundColor='white'
        image={<Image 
          style={SharedStyles.icon} 
          source={require(`NOC/img/markers-large/icon2.png`)}
        />}
        onPress={() => this._positonOnMap(this.props.station)}
      />
    }
    else if (station.icon == 25) {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/tower25.png`)}
      />;
      marker = <Button
        backgroundColor='white'
        image={<Image 
          style={SharedStyles.icon} 
          source={require(`NOC/img/markers-large/icon25.png`)}
        />}
        onPress={() => this._positonOnMap(this.props.station)}
      />
    }
    else if (station.icon == 3) {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/tower3.png`)}
      />;
      marker = <Button
        backgroundColor='white'
        image={<Image 
          style={SharedStyles.icon} 
          source={require(`NOC/img/markers-large/icon3.png`)}
        />}
        onPress={() => this._positonOnMap(this.props.station)}
      />;
    }
    else if (station.icon == 4) {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/tower4.png`)}
      />;
      marker = <Button
        backgroundColor='white'
        image={<Image 
          style={SharedStyles.icon} 
          source={require(`NOC/img/markers-large/icon4.png`)}
        />}
        onPress={() => this._positonOnMap(this.props.station)}
      />;
    }
    else {
      icon = <Image 
        style={SharedStyles.icon} 
        source={require('NOC/img/tower.png')}
      />;
      marker = <Image 
        style={SharedStyles.icon} 
        source={require(`NOC/img/markers-large/noico.png`)}
      />;
    }
    const codeName = station.code ? <TouchableWithoutFeedback 
      onPress={() => this.setState({expanded: !this.state.expanded})}
    >
      <View 
        style={[SharedStyles.cellRow, {margin: 5}]}
      >
        { icon }
        <Button 
          style={{margin: 3}} 
          onPress={() => this.setState({expanded: !this.state.expanded})}
          color='#263238'
          backgroundColor='white'
          icon={this.state.expanded ? 'expand-less' : 'expand-more'}
        />
        <Text 
          style={[SharedStyles.headingText, {flex: 1, marginTop: 10}]}
        >
          {station.code} - {station.name}
        </Text>
        { marker }
      </View>
    </TouchableWithoutFeedback> : null;
    const content = Object.keys(Const.STATIONS_LABELS).map((key, i) => {
      const data = station[Const.STATIONS_LABELS[key]];
      if (data || key == 'PRIORITET') {
        return (
          <View key={i}>
            { 
              i !== 0 ? <View 
              style={[SharedStyles.rowSeparator, {marginVertical: 5}]} 
              /> : null
            }
            <View 
              style={SharedStyles.cellRow}
            >
              <Text 
                style={SharedStyles.labelText}
              >
              {key}: 
              </Text>   
              <Text 
                style={SharedStyles.text}
              >
              {data}
              </Text>
            </View>
          </View>
        );
      }
    });
    return (
      <View style={SharedStyles.cellRow}>
        <View 
          style={[SharedStyles.block, {backgroundColor: color}]}
        />
          <View 
            style={SharedStyles.container}
          >
            { codeName }
            { 
              this.state.expanded ? <View>
                <View 
                  style={{margin: 15}}
                >
                { content }
                </View>
              </View> : null
            }
          </View>
        <View 
          style={[SharedStyles.block, {backgroundColor: color}]}
        />
      </View>
    );
  }
}

function mapDispatch(dispatch) {
  return bindActionCreators({searchMarkers}, dispatch);
}

export default connect(null, mapDispatch)(StationCell);