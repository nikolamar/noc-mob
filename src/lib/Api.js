class Api {
  static headers() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Token': 'U2QJIG0FS96V3NPSVA2F35PQD6',
    }
  }
  static get(route) {
    return this.xhr(route, null, 'GET');
  }
  static put(route, params) {
    return this.xhr(route, params, 'PUT');
  }
  static post(route, params) {
    return this.xhr(route, params, 'POST');
  }
  static delete(route, params) {
    return this.xhr(route, params, 'DELETE');
  }
  static xhr(route, params, verb) {
    const host = 'http://109.92.132.14:8080';
    const url = `${host}${route}`;
    console.info(`Api fetching: ${url}`);
    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
    options.headers = Api.headers();
    return fetch(url, options).then(resp => {
      let json = resp.json();
      if (resp.ok) {
        return json;
      }
      return json.then(err => {throw err});
    }).then(json => {
        return json;
      }
    );
  }
}
export default Api;
