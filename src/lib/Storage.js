import { Alert, AsyncStorage } from 'react-native';
import Const from 'NOC/src/lib/Const';

export default class Storage {
	static async get(query) {
	  let result = null;
	  try {
	    result = await AsyncStorage.getItem(query);
	  }
	  catch(error) {
	    Alert.alert(
	      Const.ERROR_TITLE,
	      Const.ERROR_READING_SD
	    );
	    return result;
	  }
	  return result;
	}
	static async set(key, value) {
	  try {
	    await AsyncStorage.setItem(key, value);
	  } 
	  catch(error) {
	    Alert.alert(
	      Const.ERROR_TITLE,
	      Const.ERROR_WRITING_SD
	    );
	  }
	}
	static async remove(key) {
	  try {
	    await AsyncStorage.removeItem(key);
	  } 
	  catch(error) {
	    Alert.alert(
	      Const.ERROR_TITLE,
	      Const.ERROR_WRITING_SD
	    );
	  }
	}
}