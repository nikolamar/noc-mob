export default class Stats {
  static create(items) {
    let stats = {};
    for (item of items) {
      if (item.icon) {
        if (!stats[`icon${item.icon}`]) stats[`icon${item.icon}`] = 0;
        stats[`icon${item.icon}`]++;
      }
      if (item.originalSeverity) {
        if (!stats[item.originalSeverity]) stats[item.originalSeverity] = 0;
        stats[item.originalSeverity]++;
      }
      if (item.priority >=0 && item.priority <=3) {
        if (!stats[`priority${item.priority}`]) stats[`priority${item.priority}`] = 0;
        stats[`priority${item.priority}`]++;
      }
    }
    return stats;
  }
}