import StyleVars from 'NOC/src/styles/StyleVars';

export default class Const {
	// app
  static LOADER_COLOR = StyleVars.Colors.primary;
  static LOADER_SIZE = 'large';
  static CIRCLE_RADIUS = 200;
  static NAVIGATOR_WIDTH = 200;
  static APP_TITLE = 'NOC Svet';
  static AUTHOR = 'Autor Nikola Marjanović';
  static VERSION = 'v2.0.0';
  static CONSOLE = 'Konzola';
  static THEME = 'THEME';
  static LIGHT = 'LIGHT';
  static DARK = 'DARK';
  static AUTHENTICATED = 'AUTHENTICATED';
  static PORTRAIT = 'PORTRAIT';
  static LANDSCAPE = 'LANDSCAPE';
  static EN = 'ENGLISH';
  static SR = 'SERBIAN';
  static UPDATE_CONFIRM = 'UPDATE_CONFIRM';
  static BACK_BUTTON = 'Nazad';

  // dialogs
  static UPDATE_AVAILABLE_TITLE = 'Nova verzija je dostupna';
  static MANDATORY_UPDATE_MESSAGE = 'Izašla je nova verzija koja mora da se instalira.';
  static OPTIONAL_UPDATE_MESSAGE = 'Izašla je nova verzija. Da li želite da je instalirate?';
  static SEARCH_TYPES_MESSAGE = 'K - pretraga po kodu, B - pretraga po BSC-u, I - pretraga po imenu, A - pretraga po adresi, O - pretraga po opštini';
  static CONTINUE = 'Nastavi';
  static IGNORE = 'Ignoriši';
  static INSTALL = 'Instaliraj';
  static OK = 'OK';
  static EXIT = 'Izađi';
  static CANCEL = 'Otkaži';
  static ASK_ME_LATER = 'Pitaj me kasnije';
  static DONT_ASK_AGAIN = 'Ne pitaj me više';
  static UPDATE_DONT_ASK_AGAIN_MESSAGE = 'Ne pitaj me više da ažuriram bazu.';
  static UPDATE_STATIONS_MESSAGE = 'Da li želite da ažurirate bazu stanica na vašem uređaju za bogatiji prikaz smetnji i offline režim.';
  static UPDATE_STATIONS_TITLE = 'Ažuriraj stanice';
  static ERROR_TITLE = 'Greška';
  static ERROR_MAP_POSITION = 'Ova stanica u bazi nema GPS poziciju.';
  static ERROR_FETCHING = 'Proverite internet konekciju.';
  static ERROR_READING_SD = 'Greška prilikom čitanja sa SD kartice.';
  static ERROR_WRITING_SD = 'Greška prilikom pisanja na SD karticu.';
  static SUCCESS_TITLE = 'Uspešno';
  static SUCCESS_UPDATING_STATIONS_MESSAGE = 'Bravo, uspešno ste ažurirali bazu stanica na vašem uređaju, sada možete koristi pretragu stanica bez interneta i imate bogatiji prikaz smetnji i stanica.';
  static LOGIN_EMPTY_FIELDS = 'Unesite korisničko ime i šifru.';
  static LOGIN_WELCOME_MESSAGE = 'Dobrodošli u NOC Svet, ako imate sugestija u vezi aplikacije šaljite na email nikolam@telekom.rs.';
  static LOGIN_SIGNUP_MESSAGE = 'Samo zaposlesni radnici Telekoma Srbije mogu da koriste aplikaciju sa svojim @telekom.rs e-mail nalogom, za sada ako niste zaposleni u Telekomu Srbije šaljite vaš zahtev za novi nalog administratoru na e-mail nikolam@telekom.rs';
  static LOGIN_PASSWORD_DOESNT_MATCH = 'Šifra korisnika se ne podudara';
  static LOGIN_USERNAME_PASSWORD_DOESNT_MATCH = 'Korisničko ime i šifra se ne podudaraju.';

  // USER
  static USERNAME = 'USERNAME';
  static PASS = 'PASS';
  static TOKEN = 'TOKEN';
  static PRIVILEGE = 'PRIVILEGE';

  // EXIT
  static NOTIFICATION = 'UPOZORENJE';
  static EXIT_MESSAGE = 'Da li ste sigurni da želite da napustite NOC Svet?';

	// filters & properties
  static SEVERITY = 'Statistika alarma';
  static ORIGINAL_TIME = 'originalTime';
  static DURATION_TIME_FORMATED = 'durationTimeFormated';
  static CODE = 'code';
  static SUMMARY = 'summary';
  static ADDITIONAL_INFORMATION = 'additionalInformation';
  static NODE_ALIAS = 'nodeAlias';
  static NAME = 'name';
  static TYPE = 'type';
  static ADDRESS = 'address';
  static MUNICIPALITIE = 'municipalitie';
  static REASON = 'reason';
  static PRIORITY = 'priority';
  static ASC = 'Rastuće';
  static DESC = 'Opadajuće';
  static SMALL = 'Mali';
  static MEDIUM = 'Srednji';
  static LARGE = 'Veliki';
  static CRITICAL = 'Critical';
  static MAJOR = 'Major';
  static MINOR = 'Minor';
  static WARNING = 'Warning';
  static OPENED = 'otvorene';
  static CLOSED = 'zatvorene';
  static OPENED_CLOSED = 'otvorene i zatvorene';

  // sort && search
  static CRITERIA_CODE = 'Kod';
  static CRITERIA_NAME = 'Ime';
  static CRITERIA_PRIORITY = 'Prioritet';
  static CRITERIA_TECH = 'Tehnologija';
  static CRITERIA_ADDRESS = 'Adresa';
  static CRITERIA_PLACE = 'Grad/Opština';
  static CRITERIA_PLACE_OWNER = 'Zakupodavac';

  // route types
  static ROUTE_TYPE_NORMAL = 'ROUTE_TYPE_NORMAL';
  static ROUTE_TYPE_SETTINGS = 'ROUTE_TYPE_SETTINGS';
  static ROUTE_TYPE_INFO = 'ROUTE_TYPE_INFO';

  // api url
  static URL_ALERTS = '/mts/api/alerts';
  static URL_ALERTS_SEARCH = '/mts/api/alerts?code=';
  static URL_FAILURES_SEARCH_BY_CODE = '/mts/api/failures;search?code=';
  static URL_FAILURES_SEARCH_BY_CODE_MATCH = '/mts/api/failures?code=';
  static URL_FAILURES_SEARCH_BY_NAME = '/mts/api/failures;search?name=';
  static URL_FAILURES_SEARCH_BY_NAME_MATCH = '/mts/api/failures;search?name=';
  static URL_CURRENT_FAILURES = '/mts/api/failures/current';
  static URL_STATIONS = '/mts/api/stations';
  static URL_STATIONS_SEARCH_BY_CODE = '/mts/api/stations;search?code=';
  static URL_STATIONS_SEARCH_BY_CODE_MATCH = '/mts/api/stations?code=';
  static URL_STATIONS_SEARCH_BY_NAME = '/mts/api/stations;search?name=';
  static URL_STATIONS_SEARCH_BY_NAME_MATCH = '/mts/api/stations?name=';
  static URL_STATIONS_SEARCH_BY_ADDRESS = '/mts/api/stations;search?address=';
  static URL_STATIONS_SEARCH_BY_ADDRESS_MATCH = '/mts/api/stations?address=';
  static URL_STATIONS_SEARCH_BY_MUNICIPALITIE = '/mts/api/stations;search?municipalitie=';
  static URL_STATIONS_SEARCH_BY_MUNICIPALITIE_MATCH = '/mts/api/stations?municipalitie=';

	// alerts
  static LOADING_ALERTS = 'Učitavam alarme';
	static ROUTE_ALERTS_TITLE = 'Alarmi';
	static ALL_ALERTS_URL = '/mts/api/alerts';
	static THERES_NO_SUCH_ALERT = 'Ne postoji takav alarm';
  static ALERTS_SEARCH_PLACEHOLDER = 'Pretraga alarma...';

  // settings alerts
  static ALERTS_SETTINGS = 'ALERTS_SETTINGS';
  static ALERTS_SORT_TITLE = 'Kriterijum sortiranja alarma';
  static ALERTS_SEARCH_TITLE = 'Kriterijum pretrage alarma';
  static ALERTS_FILTERS_PRIORITY_TITLE = 'Filtriraj alarme po prioritetu';
  static ALERTS_FILTERS_TECH_TITLE = 'Filtriraj alarme po tehnologiji';

	// failures
  static LOADING_FAILURES = 'Učitavam smetnje';
  static FAILURES_SORT_TITLE = 'Kriterijum sortiranja smetnji';
  static FAILURES_SEARCH_TITLE = 'Kriterijum pretrage smetnji';
	static ROUTE_FAILURES_TITLE = 'Smetnje';
	static THERES_NO_SUCH_FAILURE = 'Ne postoji takva smetnja';
	static FAILURES_SEARCH_PLACEHOLDER = 'Pretraga smetnji';
  static TECHS = 'Tehnologije';
  static PRIORITIES = 'Prioriteti';

  // settings
  static ORDER_TITLE = 'Sortiraj';

  // settings failures
  static FAILURES_SETTINGS = 'FAILURES_SETTINGS';
  static FAILURES_SORT_TITLE = 'Kriterijum sortiranja smetnji';
  static FAILURES_FILTERS_PRIORITY_TITLE = 'Filtriraj smetnje po prioritetu';
  static FAILURES_FILTERS_TECH_TITLE = 'Filtriraj smetnje po tehnologiji';

	// more info
	static ROUTE_MORE_INFO_TITLE = 'Više Informacija';

	// settings
	static ROUTE_SETTINGS_TITLE = 'Podešavanje';
  static LOADING_SETTINGS = 'Učitavam podešavanja';

	// stations
  static STATIONS_SETTINGS = 'STATIONS_SETTINGS';
	static ROUTE_STATIONS_TITLE = 'Stanice';
  static ALL_STATIONS_URL = '/mts/api/stations';
  static STATIONS_SEARCH_URL = '/mts/api/stations;search?code=';
  static LOADING_STATION = 'Učitavam stanicu.';
  static THERES_NO_SUCH_STATION = 'Ne postoji takva stanica';
  static START_SEARCHING = 'Počnite pretragu stanica';
  static STATIONS_FROM_SD = 'Pratraga sa telefona';
  static STATIONS_SEARCH_PLACEHOLDER = 'Pretraga stanica...';

  // settings stations
  static UPDATE_STATIONS = 'Ažuriraj stanice...';
  static UPDATE_STATIONS_RESUME = 'Nastavi ažuriranje';
  static UPDATE_STATIONS_TITLE_SECTION = 'Preuzmi bazu stanica';
  static NEED_TO_UPDATE = 'Potrebno je ažuriranje stanica...';
  static LAST_STATIONS_UPDATE = 'Posledji put baza je ažurirana';
  static STATIONS_SEARCH_TITLE = 'Kriterijum pretrage stanica';
  static SEARCH_CRITERIA = 'Kriterijum pretrage stanica';
  static STATIONS_SORT_TITLE = 'Kriterijum sortiranja';

  // map
  static MAP_SETTINGS = 'MAP_SETTINGS';
  static ROUTE_MAP_TITLE = 'Mapa';
  static BACK_TO_FAILURES = 'Prikaži stanice koje ne rade';

  // settings map
  static MARKER = 'Marker';
  static MAP_MARKER_SIZE_TITLE = 'Veličina markera';

	// login
  static ROUTE_LOGIN_TITLE = 'Izloguj se';
	static LOGIN = 'Prijavi se';
  static LOGIN_URL = '/mts/api/login';
  static LOGIN_SIGNUP_URL = '/mts/api/login/signup';
  static LOGIN_DO_YOU_HAVE_ACCOUNT = 'Nemate nalog?';
  static LOGIN_YOU_ALREADY_HAVE_ACCOUNT = 'Već imate nalog?';
  static LOGIN_NEW_USER = 'Prijavi novog korisnika';
  static LOGIN_PLACEHOLDER_USER = 'Korisnik';
  static LOGIN_PLACEHOLDER_PASSWORD = 'Šifra';


  // about
  static ROUTE_ABOUT_TITLE = 'Autor';

  // login
  static ROUTE_LOGIN_TITLE = 'Odjavi se';

  static ORDER_SECTION = {
    "Rastuće": false,
    "Opadajuće": true,
  };

  static TECH_SECTION = {
    "tech2G": "2G",
    "tech25G": "2.5G",
    "tech3G": "3G",
    "tech4G": "4G",
  };

  static PRIORITIES_SECTION = {
    "priority0": "0",
    "priority1": "1",
    "priority2": "2",
    "priority3": "3"
  };

  static FAILURES_SEARCH_SECTION = {
    "Kod": "code",
    "Ime": "name",
  };

  static MAP_SEARCH_SECTION = {
    "Kod": "code",
  };

  static FAILURES_SORT_SECTION = {
    "Vreme": "originalTime",
    "Kod": "code",
    "Ime": "name",
    "Prioritet": "priority",
    "Tehnologija": "tech_type",
    "Tip smetnje": "type",
    "Razlog smetnje": "reason"
  };

  static ALERTS_SORT_SECTION = {
    "Vreme": "originalTime",
    "Kod": "code",
    "Ime": "name",
    "Tip alarma": "severity",
    "Prioritet": "priority",
    "Tehnologija": "icon"
  };

  static ALERTS_SEARCH_SECTION = {
    "Kod": "code",
    "Kontroler": "nodeAlias"
  };

  static STATIONS_SEARCH_SECTION = {
    "Kod": "code",
    "Ime": "name",
    "Adresa": "address",
    "Opština": "municipalitie"
  };

  static STATIONS_SORT_SECTION = {
    "Kod": "code",
    "Ime": "name",
    "Adresa": "address",
    "Prioritet": "priority",
    "Tehnologija": "tech_type"
  };

  static STATIONS_LABELS = {
    "PRIORITET": "priority",
    "KONTROLER": "nodeAlias",
    "G. DUŽ.": "lon",
    "G. ŠIR.": "lat",
    "TEHNOLOGIJA": "tech_type",
    "KABINET": "cab_type",
    "TIP LOKACIJE": "loc_type",
    "DOBAVLJAC": "equipment_supplier",
    "OPŠTINA": "municipalitie",
    "ADRESA": "address",
    "ZAKUPODAVAC": "lessor",
    "TG": "tg",
    "TG1": "tg_1",
    "TG2": "tg_2",
    "TG3": "tg_3",
    "TG4": "tg_4",
    "TG5": "tg_5",
    "TG6": "tg_6",
    "Cell SUM": "cell_sum_3g",
    "Cell SUM 3G": "cell_sum_3g",
    "C1 TRX": "c1_trx",
    "C2 TRX": "c2_trx",
    "C3 TRX": "c3_trx",
    "C4 TRX": "c4_trx",
    "C5 TRX": "c5_trx",
    "C6 TRX": "c6_trx",
    "SCR_AIQ": "scr_aiq",
    "SCR_BJR": "scr_bjr",
    "SCR_CKS": "scr_cks",
    "SCR_DLT": "scr_dlt"
  };

  static FAILURES_LABELS = {
    "VREME": "formatedOriginalTime",
    "OTKLONJENA": "formatedLastOccurrence",
    "TRAJE": "formatedDurationTime",
    "TIP": "type",
    "RAZLOG": "reason",
    "ADRESA": "address",
    "PRIORITET": "priority"
  };

  static ALERTS_LABELS = {
    "KONTROLER": "nodeAlias",
    "ELEMENT": "node",
    "VREME": "formatedOriginalTime",
    "INFO": "summary",
    "DETALJI": "additionalInformation"
  };

  static MAP_MARKER_SIZE = {
    "Mali": "Mali",
    "Srednji": "Srednji",
    "Veliki": "Veliki"
  };

  // redux
  static REDUX_SETTINGS = 'REDUX_SETTINGS';
  static REDUX_SEARCH_FAILURES = 'REDUX_SEARCH_FAILURES';
  static REDUX_ALERTS = 'REDUX_ALERTS';
  static REDUX_SEARCH_STATIONS = 'REDUX_SEARCH_STATIONS';
  static REDUX_SEARCH_STATION_MATCH = 'REDUX_SEARCH_STATION_MATCH';
  static REDUX_FAILURES_SETTINGS = 'REDUX_FAILURES_SETTINGS';
  static REDUX_SEARCH_MARKERS = 'REDUX_SEARCH_MARKERS';
  static REDUX_GET_STORAGE = 'REDUX_GET_STORAGE';
  static REDUX_SET_STORAGE = 'REDUX_SET_STORAGE';
  static REDUX_SORT_STATIONS = 'REDUX_SORT_STATIONS';
  static REDUX_SORT_ALERTS = 'REDUX_SORT_ALERTS';
  static REDUX_SORT_FAILURES = 'REDUX_SORT_FAILURES';
  static REDUX_UPDATE_MARKERS = 'REDUX_SORT_MARKERS';

  // types
  // static TYPE = [
  //   { group: 'tip', type: 'prekid', text: 'Prekid stanice' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid linkovskog čvorišta' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad stanice' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad linkovskog čvorišta' },
  //   { group: 'tip', type: 'prekid', text: 'Servis smetnje' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid sektora' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid sektora 1' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid sektora 2' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid sektora 3' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid sektora 4' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid sektora 5' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad sektora' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad sektora 1' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad sektora 2' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad sektora 3' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad sektora 4' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad sektora 5' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid paketskog saobraćaja' },
  //   { group: 'tip', type: 'prekid', text: 'Otežan rad paketskog saobraćaja' },
  //   { group: 'tip', type: 'prekid', text: 'Prekid nadzora' },
  //   { group: 'tip', type: 'prekid', text: 'Restart stanice' },
  // ];

  // reasons
  // static REASON = [
  //   { group: 'razlog', type: 'napajanje', text: 'Nestanak napajanja' },
  //   { group: 'razlog', type: 'napajanje', text: 'Neisplaćen dug' },
  //   { group: 'razlog', type: 'napajanje', text: 'Kvar na opremi' },
  //   { group: 'razlog', type: 'napajanje', text: 'Sklopka' },
  //   { group: 'razlog', type: 'napajanje', text: 'Planirani radovi na napajanju' },
  //   { group: 'razlog', type: 'prenosni put', text: 'IP prenos' },
  //   { group: 'razlog', type: 'prenosni put', text: 'RR link' },
  //   { group: 'razlog', type: 'prenosni put', text: 'HDSL' },
  //   { group: 'razlog', type: 'prenosni put', text: 'Parice' },
  //   { group: 'razlog', type: 'prenosni put', text: 'Interni razdelnik' },
  //   { group: 'razlog', type: 'prenosni put', text: 'Planirani radovi na prenosnom putu' },
  //   { group: 'razlog', type: 'prenosni put', text: 'Kablovi' },
  //   { group: 'razlog', type: 'hardver',       text: 'Hardver' },
  //   { group: 'razlog', type: 'hardver', text: 'TRX/CU' },
  //   { group: 'razlog', type: 'hardver', text: 'BUSTER' },
  //   { group: 'razlog', type: 'hardver', text: 'CDU/DUAMCO' },
  //   { group: 'razlog', type: 'hardver', text: 'Nedostatak hardvera' },
  //   { group: 'razlog', type: 'hardver', text: 'Baterije' },
  //   { group: 'razlog', type: 'hardver', text: 'Klima' },
  //   { group: 'razlog', type: 'hardver', text: 'Visoka temperatura' },
  //   { group: 'razlog', type: 'hardver', text: 'Planirani radovi na hardveru' },
  //   { group: 'razlog', type: 'hardver', text: 'Demontirana stanica' },
  //   { group: 'razlog', type: 'hardver', text: 'Krađa opreme' },
  //   { group: 'razlog', type: 'hardver', text: 'Najavljeni radovi' },
  //   { group: 'razlog', type: 'hardver', text: 'Prekoračenje najavljenih radova' },
  //   { group: 'razlog', type: 'hardver', text: 'Lokovan sektor' },
  //   { group: 'razlog', type: 'hardver', text: 'Lokovana stanica' },
  //   { group: 'razlog', type: 'hardver', text: 'Nalog inspekcije' },
  //   { group: 'razlog', type: 'hardver', text: 'Adaptacija prostora' },
  //   { group: 'razlog', type: 'softver', text: 'Smetnje BSC' },
  //   { group: 'razlog', type: 'softver', text: 'Smetnje BS' },
  //   { group: 'razlog', type: 'softver', text: 'Planirani radovi na softveru' },
  //   { group: 'razlog', type: 'softver', text: 'Sinhronizacija' }
  // ];
}





