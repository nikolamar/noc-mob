import React, { Component } from 'react';

const Interval = ComponentToWrap => {
  return class TimeouClass extends Component {
    componentWillMount() {
      this._start();
    }
    componentWillUnmount() {
      this._stop();
    }
    _start() {
      this.interval = setInterval(() => {
        this._refresh();
      }, 60 * 1000);
    }
    _stop() {
      clearInterval(this.interval);
      this.interval = null;
    }
    _refresh() {
      if (this.props.searchFailures && this.props.failures.query.length === 0) {
        this.props.searchFailures();
      }
      if (this.props.searchAlerts && this.props.alerts.query.length === 0) {
        this.props.searchAlerts();
      }
    }
    render() {
      return (
        <ComponentToWrap 
          {...this.props}
          start={this.start}
          stop={this.stop}/>
      )
    }
  }
}

export default Interval;