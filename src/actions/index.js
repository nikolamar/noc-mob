import Const from 'NOC/src/lib/Const';
import Api from 'NOC/src/lib/Api';
import { Alert } from 'react-native';
import Storage from 'NOC/src/lib/Storage';
import Stats from 'NOC/src/lib/Stats';
import stationsJSON from 'NOC/src/json/stations';
import failuresJSON from 'NOC/src/json/failures';
import _ from 'lodash';

/*-------------------//

        MARKERS

---------------------*/

export const searchMarkers = (query = '') => {
  return async (dispatch, getState) => {
    const { markers, failures } = getState();
    let result = {};
    let settings = await Storage.get(Const.MAP_SETTINGS);
    result.query = query;
    result.settings = settings;
    if (query ==  '') {
      result[query] = createSites(failures['']);
      // result[query] = createSites(failuresJSON);
    }
    else {
      result[query] = createSites(await fetchData(Const.URL_STATIONS_SEARCH_BY_CODE_MATCH + query));
    }
    dispatch({
      type: Const.REDUX_SEARCH_MARKERS,
      payload: result
    });
  }
}

export const updateMarkers = (settings = {}) => {
  return async (dispatch, getState) => {
    const { failures, markers } = getState();
    const { query } = markers
    let result = {};
    result.query = query;
    result.settings = settings;
    dispatch({
      type: Const.REDUX_UPDATE_MARKERS,
      payload: result
    });
  }
}

function createSites(stations) {
  let markers = {};
  if (!Array.isArray(stations)) {
    Object.keys(stations).map(key => {
      if (stations[key].lat == 0 || stations[key].lat == undefined) {
        return;
      }
      let icon = 0;
      let objName = `${stations[key].lat}-${stations[key].lon}`;
      try {
        icon = Number(stations[key].icon);
      }
      catch(error) {
        console.error(error);
      }
      if (!markers[objName]) {
        markers[objName] = {};
        markers[objName].lon = stations[key].lon || 0;
        markers[objName].lat = stations[key].lat || 0;
        markers[objName].stations = [stations[key]];
        markers[objName].code = stations[key].code;
        markers[objName][icon || 0] = icon || 0;
      }
      else {
        markers[objName][icon || 0] = icon || 0;
        markers[objName].stations.push(stations[key]);
        if (stations[key].code) {
          markers[objName].code = `${markers[objName].code}, ${stations[key].code}`;
        }
      }
    });
  }
  else {
    stations.map(station => {
      if (station.lat == 0 || station.lat == undefined) {
        return;
      }
      const obj = `${station.lat}-${station.lon}`;
      let icon = 2;
      try {
        icon = Number(station.icon);
      }
      catch(error) {
        console.error(error);
      }
      if (!markers[obj]) {
        markers[obj] = {};
        markers[obj].lon = station.lon || 0;
        markers[obj].lat = station.lat || 0;
        markers[obj].stations = [station];
        markers[obj].code = station.code;
        markers[obj][icon || 0] = icon || 0;
      }
      else {
        markers[obj][icon || 0] = icon || 0;
        markers[obj].stations.push(station);
        if (station.code) {
          markers[obj].code = `${markers[obj].code}, ${station.code}`;
        }
      }
    });
  }
  return markers;
}

/*-------------------//

        FAILURES

---------------------*/

export const searchFailures = (query = '') => {
  return async (dispatch, getState) => {
  	const { failures } = getState();
    let result = {};
    result.query = query;
    let settings = await Storage.get(Const.FAILURES_SETTINGS);
    if (query ==  '') {
      result[query] = await fetchData(Const.URL_CURRENT_FAILURES);
    }
    if (query.length > 2) {
      if (settings) {
        const set = JSON.parse(settings);
        if (set.searchProperty === Const.NAME) {
          result[query] = await fetchData(Const.URL_FAILURES_SEARCH_BY_NAME + query);
        }
        else {
          result[query] = await fetchData(Const.URL_FAILURES_SEARCH_BY_CODE_MATCH + query);
        }
      }
    }
    result[query] = await mergeData(result[query]);
    if (settings) {
      const set = JSON.parse(settings);
      const order = set.sortProperty == 'originalTime' ? set.isAsc ? 'desc' : 'asc' : set.isAsc ? 'asc' : 'desc';
      result[query] = _.orderBy(result[query], set.sortProperty, order);
    }
    dispatch({
	    type: Const.REDUX_SEARCH_FAILURES,
	    payload: result
  	});
  }
}

export const sortFailures = (settings = {}) => {
  return async (dispatch, getState) => {
  	const { failures } = getState();
    const { query } = failures
    const order = settings.sortProperty == 'originalTime' ? settings.isAsc ? 'desc' : 'asc' : settings.isAsc ? 'asc' : 'desc';
	  let result = {};
	  result[query] = _.orderBy(failures[query], settings.sortProperty, order);
    result.query = query;
    dispatch({
	    type: Const.REDUX_SORT_FAILURES,
	    payload: result
  	});
  }
}

/*-------------------//

        ALERTS

---------------------*/

export const searchAlerts = (query = '') => {
  return async (dispatch, getState) => {
  	const { alerts } = getState();
    let result = {};
    result.query = query;
    if (query ==  '') {
      result[query] = await fetchData(Const.URL_ALERTS);
    }
    else if (query.length > 2) {
      let settings = await Storage.get(Const.ALERTS_SETTINGS);
      if (settings) {
        // alert(settings);
        settings = JSON.parse(settings);
        switch (settings.searchProperty) {
          case Const.CODE:
            result[query] = _.filter(alerts[''], alert => alert.code == query);
            break;
          case Const.NODE_ALIAS:
            result[query] = _.filter(alerts[''], alert => alert.nodeAlias == query);
            break;
          case Const.SUMMARY:
            result[query] = _.filter(alerts[''], alert => alert.summary == query);
            break;
          case Const.ADDITIONAL_INFORMATION:
            result[query] = _.filter(alerts[''], alert => alert.additionalInformation == query);
            break;
          default:
            result[query] = _.filter(alerts[''], alert => alert.code == query);
        }
      }
      else {
        result[query] = _.filter(alerts[''], alert => alert.code == query);
      }
      // result[query] = await fetchData(Const.URL_ALERTS_SEARCH + query);
    }
	  result[query] = await mergeData(result[query]);
    let settings = await Storage.get(Const.ALERTS_SETTINGS);
    if (settings) {
      settings = JSON.parse(settings);
      const order = settings.sortProperty == 'originalTime' ? settings.isAsc ? 'desc' : 'asc' : settings.isAsc ? 'asc' : 'desc';
      result[query] = _.orderBy(result[query], settings.sortProperty, order);
      result.query = query;
    }
    dispatch({
	    type: Const.REDUX_ALERTS,
	    payload: result
  	});
  }
}

export const sortAlerts = (settings = {}) => {
  return async (dispatch, getState) => {
  	const { alerts } = getState();
    const { query } = alerts;
    const order = settings.sortProperty == 'originalTime' ? settings.isAsc ? 'desc' : 'asc' : settings.isAsc ? 'asc' : 'desc';
	  let result = {};
	  result[query] = _.orderBy(alerts[query], settings.sortProperty, order);
    result.query = alerts.query;
    dispatch({
	    type: Const.REDUX_SORT_ALERTS,
	    payload: result
  	});
  }
}

/*-------------------//

        STATIONS

---------------------*/

export const searchStation = (query = '') => {
  return async (dispatch, getState) => {
    const { stations } = getState();
    let result = {};
    result.query = query;
    result[query] = await fetchData(Const.URL_STATIONS_SEARCH_MATCH + query);
    dispatch({
      type: Const.REDUX_SEARCH_STATIONS,
      payload: result
    });
  }
}

export const searchStations = (query = '') => {
  return async (dispatch, getState) => {
  	const { stations } = getState();
    let result = {};
    result.query = query;
    let settings = await Storage.get(Const.STATIONS_SETTINGS);
    if (query ==  '') {
      result[query] = await fetchData(Const.URL_STATIONS);
    }
    if (query.length > 2) {
      let url = Const.URL_STATIONS_SEARCH_BY_CODE;
      let settings = await Storage.get(Const.STATIONS_SETTINGS);
      if (settings) {
        const set = JSON.parse(settings);
        switch (set.searchProperty) {
          case Const.NAME:
            url = Const.URL_STATIONS_SEARCH_BY_NAME;
            break;
          case Const.ADDRESS:
            url = Const.URL_STATIONS_SEARCH_BY_ADDRESS;
            break;
          case Const.MUNICIPALITIE:
            url = Const.URL_STATIONS_SEARCH_BY_MUNICIPALITIE;
            break;
          default:
            url = Const.URL_STATIONS_SEARCH_BY_CODE;
        }
      }
      result[query] = await fetchData(url + query);
    }
    if (settings) {
      const set = JSON.parse(settings);
      const order = set.sortProperty == 'originalTime' ? set.isAsc ? 'desc' : 'asc' : set.isAsc ? 'asc' : 'desc';
      result[query] = _.orderBy(result[query], set.sortProperty, order);
    }
    dispatch({
	    type: Const.REDUX_SEARCH_STATIONS,
	    payload: result
  	});
  }
}

export const sortStations = (settings = {}) => {
  return async (dispatch, getState) => {
  	const { stations } = getState();
    const { query } = stations;
    const order = settings.sortProperty == 'originalTime' ? settings.isAsc ? 'desc' : 'asc' : settings.isAsc ? 'asc' : 'desc';
	  let result = {};
	  result[query] = _.orderBy(stations[query], settings.sortProperty, order);
    result.query = query;
    dispatch({
	    type: Const.REDUX_SORT_STATIONS,
	    payload: result
  	});
  }
}

async function fetchData(query) {
	let result = [];
  try {	
		result = await Api.get(query);
	}
	catch(error) {
    Alert.alert(
	    Const.ERROR_TITLE,
	    Const.ERROR_FETCHING
    );
    return result;
	}
	return result;
}

async function mergeData(data) {
  let merged = [];
  for (let i in data) {
    let station = {};
    const stationSD = await Storage.get(data[i].code);
    if (stationSD) {
      station = JSON.parse(stationSD);
    } else {
      station = stationsJSON[data[i].code];
    }
    // TODO: FIX => REPLACE VAR MISLEADING NODE IN ALERT
    if (station) data[i].nodeAlias = station.node;
    merged[i] = Object.assign({}, station, data[i]);
  }
  return merged;
}