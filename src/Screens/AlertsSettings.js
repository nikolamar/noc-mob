import React, { Component } from 'react';
import {
  Picker,
  StyleSheet,
  ScrollView,
  ListView,
  View,
  Text,
  Switch
} from 'react-native';
import StyleVars from 'NOC/src/styles/StyleVars';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import Button from 'NOC/src/Views/Button';
import Const from 'NOC/src/lib/Const';
import Storage from 'NOC/src/lib/Storage';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { sortAlerts } from 'NOC/src/actions';

const styles = StyleSheet.create({
  margin: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    marginBottom: 20
  },
  buttonSwitch: {
    margin: 5,
    flex: 1
  },
  buttonOn: {
    backgroundColor: StyleVars.Colors.primary
  },
  buttonOff: {
    backgroundColor: StyleVars.Colors.secondaryText
  }
});

class AlertsSettings extends Component {
  state = {
    tech2G: true,
    tech25G: true,
    tech3G: true,
    tech4G: true,
    priority0: true,
    priority1: true,
    priority2: true,
    priority3: true,
    isAsc: true,
    searchProperty: Const.CODE,
    sortProperty: Const.ORIGINAL_TIME
  }
  _handleButtons(key) {
    this.setState({
      key: this.state[key] = !this.state[key]
    });
  }
  _handlePickerOrder(key) {
    this.setState({
      isAsc: key
    });
  }
  _handlePickerSearch(key) {
    this.setState({
      searchProperty: key
    });
  }
  _handlePickerSort(key) {
    this.setState({
      sortProperty: key
    });
  }
  async componentWillMount() {
    const settings = await Storage.get(Const.ALERTS_SETTINGS);
    if (settings) {
      this.setState(JSON.parse(settings));
    }
  }
  componentWillUnmount() {
    Storage.set(Const.ALERTS_SETTINGS, JSON.stringify(this.state));
    this.props.sortAlerts(this.state);
  }
  render() {
    const orderPick = Object.keys(Const.ORDER_SECTION).map(key =>
      <Picker.Item
        key={key} 
        label={key}
        value={Const.ORDER_SECTION[key]} 
      />
    );
    const searchPick = Object.keys(Const.ALERTS_SEARCH_SECTION).map(key =>
      <Picker.Item
        key={key} 
        label={key}
        value={Const.ALERTS_SEARCH_SECTION[key]} 
      />
    );
    const sortPick = Object.keys(Const.ALERTS_SORT_SECTION).map(key =>
      <Picker.Item
        key={key} 
        label={key}
        value={Const.ALERTS_SORT_SECTION[key]} 
      />
    );
    const techButtons = Object.keys(Const.TECH_SECTION).map(key =>
      <Button
        key={key}
        onPress={() => this._handleButtons(key)}
        style={[
          styles.buttonSwitch,
          this.state[key] ? styles.buttonOn : styles.buttonOff
        ]}>
        {Const.TECH_SECTION[key]}
      </Button>
    );
    const priorityButtons = Object.keys(Const.PRIORITIES_SECTION).map(key =>
      <Button
        key={key}
        onPress={() => this._handleButtons(key)}
        style={[
          styles.buttonSwitch,
          this.state[key] ? styles.buttonOn : styles.buttonOff
        ]}>
        {Const.PRIORITIES_SECTION[key]}
      </Button>
    );
    const content = <ScrollView style={SharedStyles.container}>
      <View 
        style={SharedStyles.rowSeparator}
      />
      <Text 
        style={[SharedStyles.headingText, styles.margin]}>
        {Const.ORDER_TITLE}
      </Text>
      <View 
        style={SharedStyles.cellRow}
      >
        <Picker
          style={{flex: 1}}
          selectedValue={this.state.isAsc}
          onValueChange={this._handlePickerOrder.bind(this)}>
          { orderPick }
        </Picker>
      </View>
      <View 
        style={SharedStyles.rowSeparator}
      />
      <Text 
        style={[SharedStyles.headingText, styles.margin]}>
        {Const.ALERTS_SEARCH_TITLE}
      </Text>
      <View 
        style={SharedStyles.cellRow}
      >
        <Picker
          style={{flex: 1}}
          selectedValue={this.state.searchProperty}
          onValueChange={this._handlePickerSearch.bind(this)}>
          { searchPick }
        </Picker>
      </View>
      <View 
        style={SharedStyles.rowSeparator}
      />
      <Text 
        style={[SharedStyles.headingText, styles.margin]}>
        {Const.ALERTS_SORT_TITLE}
      </Text>
      <View 
        style={SharedStyles.cellRow}
      >
        <Picker
          style={{flex: 1}}
          selectedValue={this.state.sortProperty}
          onValueChange={this._handlePickerSort.bind(this)}>
          { sortPick }
        </Picker>
      </View>
    </ScrollView>;
    return (
      <View 
        style={SharedStyles.container}
      >
        { content }
        <View style={SharedStyles.rowSeparator} />
      </View>
    )
  }
}

function mapState({storage}) {
  return { storage };
}

function mapDispatch(dispatch) {
  return bindActionCreators({sortAlerts}, dispatch);
}

export default connect(mapState, mapDispatch)(AlertsSettings);
