import React, { Component, PropTypes } from 'react';
import {
  UIManager,
  LayoutAnimation,
  ScrollView,
  ActivityIndicator,
  Alert,
  ListView,
  View,
  Text,
  RefreshControl
} from 'react-native';
import CustomLayoutAnimation from 'NOC/src/lib/CustomLayoutAnimation';
import Storage from 'NOC/src/lib/Storage';
import StyleVars from 'NOC/src/styles/StyleVars';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import SearchBar from 'NOC/src/Views/SearchBar';
import StationCell from 'NOC/src/Views/StationCell';
import Const from 'NOC/src/lib/Const';
import { connect } from 'react-redux';
import { searchStations } from 'NOC/src/actions/index';
import { bindActionCreators } from 'redux';
import dismissKeyboard from 'dismissKeyboard';

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Stations extends Component {
  static propTypes = {
    stations: PropTypes.object,
  }
  dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  cache = [];
  state = {
    isLoading: false,
    refreshing: false,
    dataSource: this.dataSource.cloneWithRows([]),
    settings: {},
  };
  _renderRow = (station, sectionID, rowID, highlightRow) => (
    <StationCell
      navigator={this.props.navigator}
      station={station}
      onHighlight={() => highlightRow(sectionID,rowID)}
      onDeHighlight={() => highlightRow(null,null)}
    />
  )
  _renderSeparator = (sectionID, rowID, adjacentRowHighlighted) => (
    <View
      key={`${sectionID}${rowID}`}
      style={[{padding: 3}, SharedStyles.rowSeparator, adjacentRowHighlighted && SharedStyles.rowSeparatorHighlighted]}
    />
  )
  _onRefresh() {
    this.props.searchStations(this.props.stations.query);
  }
  _search(query = '') {
    clearTimeout(this.timeoutID);
    if (query.length < 3) {
      this.timeoutID = null;
      this.setState({
        isLoading: false,
        dataSource: this.dataSource.cloneWithRows([])
      });
      return;
    }
    if (query.length > 3) {
      this.setState({
        isLoading: true
      });
    }
    if (this.cache[query]) {
      this.timeoutID = null;
      this.setState({
        dataSource: this.dataSource.cloneWithRows(this.cache[query])
      });
      return;
    }
    this.timeoutID = setTimeout(() => this.props.searchStations(query), 250);
    this.setState({isLoading: true});
  }
  _clearText() {
    dismissKeyboard();
    clearTimeout(this.timeoutID);
    this.setState({
      isLoading: false,
      refreshing: false,
      dataSource: this.dataSource.cloneWithRows([])
    });
  }
  _getSettings = async () => {
    const settings = await Storage.get(Const.STATIONS_SETTINGS);
    if (settings) {
      this.setState({settings: JSON.parse(settings)});
    }
  }
  componentWillMount() {
    this._getSettings();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.stations[nextProps.stations.query] && this.timeoutID) {
      // this.cache[nextProps.stations.query] = nextProps.stations[nextProps.stations.query];
      this.setState({
        isLoading: false,
        refreshing: false,
        dataSource: this.dataSource.cloneWithRows(nextProps.stations[nextProps.stations.query])
      });
    }
    this._getSettings();
  }
  componentWillUpdate() {
    LayoutAnimation.configureNext(CustomLayoutAnimation);
    // LayoutAnimation.linear();
  }
  componentWillUnmount() {
    clearTimeout(this.timeoutID);
  }
  render() {
    let content = null;
    if (this.state.isLoading) {
      content = <View style={SharedStyles.emptyList}>
        <ActivityIndicator 
          animating={true} 
          size='large' 
          color={StyleVars.Colors.primary}
        />
      </View>;
    } 
    else if (this.state.dataSource.getRowCount() === 0) {
      content = <ScrollView keyboardDismissMode='interactive'>
        <View 
          style={SharedStyles.emptyList}
        >
          <Text 
            style={SharedStyles.emptyListText}>
            {Const.THERES_NO_SUCH_STATION}
          </Text>
        </View>
      </ScrollView>
    }
    else {
      content = <ListView
        refreshControl={<RefreshControl
          colors={[StyleVars.Colors.primary]}
          tintColor ={StyleVars.Colors.primary}
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}/>}
        initialListSize={5}
        pageSize={10}
        enableEmptySections={true}
        dataSource={this.state.dataSource}
        renderSeparator={this._renderSeparator}
        renderRow={this._renderRow}
        automaticallyAdjustContentInsets={false}
        keyboardDismissMode='on-drag'/>;
    }
    const search = this.props.isSearchVisible ? <SearchBar
      settings={this.state.settings}
      placeholder={Const.STATIONS_SEARCH_PLACEHOLDER}
      isLoading={this.state.isLoading}
      clearText={this._clearText.bind(this)}
      onSearch={e => this._search(e.nativeEvent.text.toUpperCase())}
    /> : null;
  	return (
      <View 
        style={SharedStyles.container}
      >
		  	{ search }
	      { content }
		  </View>
	  )
  }
}

function mapState({stations}) {
  return { stations }
}

function mapDispatch(dispatch) {
  return bindActionCreators({ searchStations }, dispatch);
}

export default connect(mapState, mapDispatch)(Stations);