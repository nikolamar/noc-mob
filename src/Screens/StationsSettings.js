import React, { Component } from 'react';
import {
  StyleSheet,
  Switch,
  Picker,
  ScrollView,
  Alert,
  View,
  Text,
  ActivityIndicator
} from 'react-native';
import dateFormat from 'dateformat';
import StyleVars from 'NOC/src/styles/StyleVars';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import Button from 'NOC/src/Views/Button';
import Const from 'NOC/src/lib/Const';
import Storage from 'NOC/src/lib/Storage';
import Api from 'NOC/src/lib/Api';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { sortStations } from 'NOC/src/actions';

const styles = StyleSheet.create({
  margin: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    marginBottom: 20
  },
  buttonSwitch: {
    margin: 5,
    flex: 1
  },
  buttonOn: {
    backgroundColor: StyleVars.Colors.primary
  },
  buttonOff: {
    backgroundColor: StyleVars.Colors.secondaryText
  }
});

class StationsSettings extends Component {
  state = {
    isLoading: false,
    isSavingItems: false,
    itemsLength: 0,
    searchingStorage: false,
    currentItem: 0,
    lastUpdateTime: null,
    tech2G: true,
    tech25G: true,
    tech3G: true,
    tech4G: true,
    priority0: true,
    priority1: true,
    priority2: true,
    priority3: true,
    isAsc: true,
    isMatch: false,
    searchProperty: Const.CODE,
    sortProperty: Const.CODE
  }
  async componentWillMount() {
    const settings = await Storage.get(Const.STATIONS_SETTINGS);
    if (settings) {
      this.setState(JSON.parse(settings));
    }
  }
  async saveStations(items) {
    let i = this.state.currentItem;
    while (i < items.length) {
      if (this.stopSavingStations) return;
      this.setState({currentItem: i});
      await Storage.set(items[i].code, JSON.stringify(items[i]));
      i++;
    }
    const lastUpdateTime = JSON.stringify(new Date());
    this.setState({
      isSavingItems: false, 
      currentItem: 0, 
      itemsLength: 0,
      lastUpdateTime: lastUpdateTime
    });
    Alert.alert(
      Const.SUCCESS_TITLE,
      Const.SUCCESS_UPDATING_STATIONS_MESSAGE
    );
  }
  componentWillUnmount() {
    this.stopSavingStations = true;
    let settings = JSON.stringify({
      searchingStorage: this.state.searchingStorage,
      currentItem: this.state.currentItem,
      lastUpdateTime: this.state.lastUpdateTime,
      tech2G: this.state.tech2G,
      tech25G: this.state.tech25G,
      tech3G: this.state.tech3G,
      tech4G: this.state.tech4G,
      priority0: this.state.priority0,
      priority1: this.state.priority1,
      priority2: this.state.priority2,
      priority3: this.state.priority3,
      isAsc: this.state.isAsc,
      isMatch: this.state.isMatch,
      searchProperty: this.state.searchProperty,
      sortProperty: this.state.sortProperty
    });
    Storage.set(Const.STATIONS_SETTINGS, settings);
    this.props.sortStations(this.state);
  }
  handleButtons(key) {
    this.setState({
      key: this.state[key] = !this.state[key]
    });
  }
  _handlePickerOrder(key) {
    this.setState({
      isAsc: key
    });
  }
  handlePickerSearch(key) {
    this.setState({
      searchProperty: key
    });
  }
  handlePickerSort(key) {
    this.setState({
      sortProperty: key
    });
  }
  async update() {
    this.setState({ isLoading: true });
    stations = await Api.get(Const.URL_STATIONS);
    this.setState({ isLoading: false });
    if (stations) {
      this.setState({itemsLength: stations.length, isSavingItems: true});
      await this.saveStations(stations);
    }
  }
  render() {
    let updateStations = null;
    if (this.state.isLoading) {
      updateStations = <ActivityIndicator
        color={Const.LOADER_COLOR}
        animating={this.state.isLoading || this.state.isSavingItems}
        style={SharedStyles.loader}
        size='small'/>
    }
    else if (!this.state.isSavingItems && this.state.currentItem != 0) {
      updateStations = <Button onPress={this.update.bind(this)}>{Const.UPDATE_STATIONS_RESUME}</Button>
    }
    else if (this.state.isSavingItems) {
      updateStations = <Text style={[SharedStyles.headingText, {color: StyleVars.Colors.primary}]}>{this.state.currentItem}/{this.state.itemsLength}</Text>;
    }
    else {
      updateStations = <Button onPress={this.update.bind(this)}>{Const.UPDATE_STATIONS}</Button>;
    }
    const searchPick = Object.keys(Const.STATIONS_SEARCH_SECTION).map(key => 
      <Picker.Item 
        key={key} 
        label={key} 
        value={Const.STATIONS_SEARCH_SECTION[key]} 
      />
    );
    const orderPick = Object.keys(Const.ORDER_SECTION).map(key =>
      <Picker.Item
        key={key} 
        label={key}
        value={Const.ORDER_SECTION[key]} 
      />
    );
    const sortPick = Object.keys(Const.STATIONS_SORT_SECTION).map(key => 
      <Picker.Item 
        key={key} 
        label={key} 
        value={Const.STATIONS_SORT_SECTION[key]} 
      />
    );
    const techButtons = Object.keys(Const.TECH_SECTION).map(key =>
      <Button
        key={key}
        onPress={() => this.handleButtons(key)} 
        style={[
          styles.buttonSwitch, 
          this.state[key] ? styles.buttonOn : styles.buttonOff
        ]}>
        {Const.TECH_SECTION[key]}
      </Button>
    );
    const priorityButtons = Object.keys(Const.PRIORITIES_SECTION).map(key =>
      <Button
        key={key}
        onPress={() => this.handleButtons(key)} 
        style={[
          styles.buttonSwitch, 
          this.state[key] ? styles.buttonOn : styles.buttonOff
        ]}>
        {Const.PRIORITIES_SECTION[key]}
      </Button>
    );
    const content = <ScrollView style={SharedStyles.container}>
      <View>
        <View style={[SharedStyles.cellRow, styles.margin, {alignItems: 'flex-end', height: 60}]}>
          <Text style={[SharedStyles.headingText,{flex: 1}]}>{Const.UPDATE_STATIONS_TITLE_SECTION}</Text>
          { updateStations }
        </View>
        <Text style={[SharedStyles.text, styles.margin, {textAlign: 'center'}]}>
          { this.state.lastUpdateTime ? `${Const.LAST_STATIONS_UPDATE} ${dateFormat(JSON.parse(this.state.lastUpdateTime), 'HH:MM:ss - dd/mm/yyyy')}` : Const.NEED_TO_UPDATE }
        </Text>
      </View>
      <View style={SharedStyles.rowSeparator}/>
      <View style={[SharedStyles.cellRow, styles.margin, {alignItems: 'flex-end'}]}>
        <Text style={[SharedStyles.headingText, {flex: 1}]}>{Const.STATIONS_FROM_SD}</Text>
        <Switch
          onValueChange={() => this.setState({searchingStorage: !this.state.searchingStorage})}
          onTintColor={StyleVars.Colors.primary}
          value={this.state.searchingStorage}/>
      </View>
      <View style={SharedStyles.rowSeparator}/>
      <Text style={[SharedStyles.headingText, styles.margin]}>{Const.ORDER_TITLE}</Text>
      <View style={SharedStyles.cellRow}>
        <Picker
          style={{flex: 1}}
          selectedValue={this.state.isAsc}
          onValueChange={this._handlePickerOrder.bind(this)}>
          { orderPick }
        </Picker>
      </View>
      <View style={SharedStyles.rowSeparator}/>
      <View>
        <Text style={[SharedStyles.headingText, styles.margin]}>{Const.STATIONS_SEARCH_TITLE}</Text>
        <Picker
          style={{flex: 1}}
          selectedValue={this.state.searchProperty}
          onValueChange={this.handlePickerSearch.bind(this)}>
          { searchPick }
        </Picker>
      </View>
      <View style={SharedStyles.rowSeparator}/>
      <View>
        <Text style={[SharedStyles.headingText, styles.margin]}>{Const.STATIONS_SORT_TITLE}</Text>
        <Picker
          style={{flex: 1}}
          selectedValue={this.state.sortProperty}
          onValueChange={this.handlePickerSort.bind(this)}>
          { sortPick }
        </Picker>
      </View>
    </ScrollView>;
    return (
      <View 
        style={SharedStyles.container}
      >
        { content }
        <View style={SharedStyles.rowSeparator} />
      </View>
    )
  }
}

function mapDispatch(dispatch) {
  return bindActionCreators({sortStations}, dispatch);
}

export default connect(null, mapDispatch)(StationsSettings);