import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Picker
} from 'react-native';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import Const from 'NOC/src/lib/Const';
import Storage from 'NOC/src/lib/Storage';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { updateMarkers } from 'NOC/src/actions';

const styles = StyleSheet.create({
  margin: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
    marginBottom: 20
  },
});

class MapSettings extends Component {
  state = {
    size: Const.LARGE,
    searchProperty: Const.CODE,
  }
  _handlePicker(key) {
    this.setState({
      size: key
    });
  }
  _handlePickerSearch(key) {
    this.setState({
      searchProperty: key
    });
  }
  async componentWillMount() {
    const settings = await Storage.get(Const.MAP_SETTINGS);
    if (settings) {
      this.setState(JSON.parse(settings));
    }
  }
  componentWillUnmount() {
    Storage.set(Const.MAP_SETTINGS, JSON.stringify(this.state));
    this.props.updateMarkers(this.state);
  }
  render() {
    const sizePick = Object.keys(Const.MAP_MARKER_SIZE).map(key => 
      <Picker.Item 
        key={key} 
        label={key} 
        value={Const.MAP_MARKER_SIZE[key]} 
      />
    );
    const searchPick = Object.keys(Const.MAP_SEARCH_SECTION).map(key =>
      <Picker.Item
        key={key} 
        label={key}
        value={Const.MAP_SEARCH_SECTION[key]} 
      />
    );
  	return (
      <View 
        style={SharedStyles.container}
      >
        <Text 
          style={[SharedStyles.headingText, styles.margin]}>
          {Const.FAILURES_SEARCH_TITLE}
        </Text>
        <View 
          style={SharedStyles.cellRow}
        >
          <Picker
            style={{flex: 1}}
            selectedValue={this.state.searchProperty}
            onValueChange={this._handlePickerSearch.bind(this)}>
            { searchPick }
          </Picker>
        </View>
        <View 
          style={SharedStyles.rowSeparator}
        />
        <Text 
          style={[SharedStyles.headingText, styles.margin]}>
          {Const.MAP_MARKER_SIZE_TITLE}
        </Text>
        <View 
          style={SharedStyles.cellRow}
        >
          <Picker
            style={{flex: 1}}
            selectedValue={this.state.size}
            onValueChange={this._handlePicker.bind(this)}>
            { sizePick }
          </Picker>
        </View>
		  </View>
	  )
  }
}

function mapDispatch(dispatch) {
  return bindActionCreators({updateMarkers}, dispatch);
}

export default connect(null, mapDispatch)(MapSettings);