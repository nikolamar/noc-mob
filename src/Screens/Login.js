import React, { Component, PropTypes } from 'react';
import {
  Animated,
  Easing,
  UIManager,
  LayoutAnimation,
  Alert,
  Dimensions,
  Image,
  StyleSheet,
  ActivityIndicator,
  Text,
  TextInput,
  ScrollView,
  View,
  Keyboard,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
} from 'react-native';
import CustomLayoutAnimation from 'NOC/src/lib/CustomLayoutAnimation';
import Api from 'NOC/src/lib/Api';
import Button from 'NOC/src/Views/Button';
import StyleVars from 'NOC/src/styles/StyleVars';
import Const from 'NOC/src/lib/Const';
import dismissKeyboard from 'dismissKeyboard';

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: StyleVars.Colors.primary
  },
  loader: {
    position: 'absolute',
    bottom: 30,
    left: 0,
    right: 0,
    marginBottom: 20
  },
  author: {
    position: 'absolute',
    color: 'white',
    left: 0,
    right: 0,
    bottom: 10,
    textAlign: 'center',
    fontSize: 10,
    fontWeight: '400',
    fontFamily: StyleVars.Fonts.general
  },
  input: {
    width: 250,
    margin: 10,
    padding: 15,
    height: 60,
    backgroundColor: StyleVars.Colors.primary,
    color: 'white',
    fontFamily: StyleVars.Fonts.general,
    fontSize: 14,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'rgba(255,255,255,0.75)',
    opacity: 0.85,
  },
  loginButton: {
    paddingVertical: 10,
    margin: 10,
    backgroundColor: 'rgba(255,255,255,0.25)',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'rgba(255,255,255,0.75)',
    opacity: 0.85,
  }
})

export default class Login extends Component {
  state = {
    behavior: 'padding',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    orientation: Dimensions.get('window').width > Dimensions.get('window').height ? Const.LANDSCAPE : Const.PORTRAIT,
    username: this.props.username || '',
    password: '',
    isLoading: false,
    flex: 1,
    spinValue: new Animated.Value(0),
  }
  _onLayout(event) {
    console.info('changing layout')
    const { width, height } = event.nativeEvent.layout;
    const orientation = width > height ? Const.LANDSCAPE : Const.PORTRAIT;
    this.setState({width, height, orientation});
  }
  async _submitForm() {
    if (!this.state.username || !this.state.password) {
      Alert.alert(
        Const.ERROR_TITLE,
        Const.LOGIN_EMPTY_FIELDS
      );
      return;
    }
    this.setState({isLoading: true});
    let resp = null;
    try {
      resp = await Api.post(Const.LOGIN_URL, {
        user: this.state.username,
        pass: this.state.password
      });
    }
    catch(error) {
      Alert.alert(
        Const.ERROR_TITLE,
        Const.ERROR_FETCHING
      );
    }

    if (resp) {
      this.props.authenticate({
        user: this.state.username,
        pass: this.state.password,
        token: resp.token,
        privilege: resp.privilege
      });
      return;
    }
    this.setState({isLoading: false});
  }
  _changeSignup() {
    this.setState({isSignup: !this.state.isSignup});
  }
  _keyboardDidShow() {
    this.setState({flex: 3});
  }
  _keyboardDidHide() {
    this.setState({flex: 1});
  }
  componentWillReceiveProps({ username }) {
    if (username) {
      this.setState({username});
    }
  }
  componentWillUpdate() {
    LayoutAnimation.configureNext(CustomLayoutAnimation);
  }
  componentWillMount () {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
  }
  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  componentDidMount() {
    this.state.spinValue.setValue(0);
    Animated.timing(
      this.state.spinValue,
      {
        toValue: 1,
        duration: 100000,
        easing: Easing.linear
      }
    ).start();
  }
  render() {
    const rotate = this.state.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '720deg']
    });
    const logo = this.state.orientation === Const.PORTRAIT ? <View
        style={{
          flex: 1, 
          alignItems: 'center', 
          justifyContent: 'center'
        }}
      >
      <Animated.Image
        style={{transform: [{ rotate }] }}
        source={require('NOC/img/logo.png')} 
        resizeMode='stretch'
      />
      <Text 
        style={{
          position: 'absolute',
          flex: 1,
          fontSize: 20,
          fontWeight: '600',
          color: 'white',
        }}
      >
        NOC Svet
      </Text>
    </View> : null;
    return (
      <TouchableWithoutFeedback
        onLayout={this._onLayout.bind(this)}
        onPress={() => dismissKeyboard()}
      >
        <View 
          style={[styles.container, {alignItems: 'center'}]}
        >
          { logo }
          <KeyboardAvoidingView 
            behavior={this.state.behavior}
            style={{flex: this.state.flex, margin: 20}}
          >
            <View 
              style={this.state.orientation !== Const.PORTRAIT ? {flexDirection: 'row'} : null}
            >
              <TextInput
                underlineColorAndroid={StyleVars.Colors.primary}
                placeholder={Const.LOGIN_PLACEHOLDER_USER}
                placeholderTextColor='rgba(255,255,255,0.75)'
                keyboardType='email-address'
                selectionColor='white'
                style={styles.input}
                autoCapitalize='none'
                autoCorrect={false}
                onChangeText={username => this.setState({username})}
                value={this.state.username}
                returnKeyType='next'
                onSubmitEditing={() => this.passwordRef.focus()}
              />
              <TextInput
                ref={ref => this.passwordRef = ref}
                underlineColorAndroid={StyleVars.Colors.primary}
                placeholder={Const.LOGIN_PLACEHOLDER_PASSWORD}
                placeholderTextColor='rgba(255,255,255,0.75)'
                secureTextEntry={true}
                selectionColor='white'
                style={styles.input}
                autoCapitalize='none'
                autoCorrect={false}
                onChangeText={password => this.setState({password})}
                returnKeyType={this.state.isSignup ? 'next' : 'go'}
                onSubmitEditing={() => this.state.isSignup ? this.passwordConfirmationRef.focus() : this._submitForm()}
              />
            </View>
            <Button
              onPress={this._submitForm.bind(this)}
              textStyle={{fontSize: 14}}
              style={styles.loginButton}>
              {Const.LOGIN}
            </Button>
          </KeyboardAvoidingView>
          {
            this.state.isLoading ? <ActivityIndicator size='large' animating={true} color='white' style={styles.loader}/> : null
          }
          <Text 
            style={styles.author}
          >
            {Const.AUTHOR}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
