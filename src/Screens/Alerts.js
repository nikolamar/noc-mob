import React, { Component, PropTypes } from 'react';
import {
  UIManager,
  LayoutAnimation,
  ToastAndroid,
  ScrollView,
	ListView,
	View,
	Text,
  ActivityIndicator,
  RefreshControl,
  Platform
} from 'react-native';
import CustomLayoutAnimation from 'NOC/src/lib/CustomLayoutAnimation';
import Storage from 'NOC/src/lib/Storage';
import StyleVars from 'NOC/src/styles/StyleVars';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import dateFormat from 'dateformat';
import Routes from 'NOC/src/Routes';
import SearchBar from 'NOC/src/Views/SearchBar';
import Button from 'NOC/src/Views/Button';
import AlertCell from 'NOC/src/Views/AlertCell';
import Stats from 'NOC/src/lib/Stats';
import StatsView from 'NOC/src/Views/StatsView';
import Const from 'NOC/src/lib/Const';
import Interval from 'NOC/src/lib/Interval';
import stations from 'NOC/src/json/stations.json';
import { connect } from 'react-redux';
import { searchAlerts } from 'NOC/src/actions/index';
import { bindActionCreators } from 'redux';
import dismissKeyboard from 'dismissKeyboard';

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Alerts extends Component {
  static propTypes = {
    alerts: PropTypes.object,
    isAndroid: PropTypes.bool,
  }
  static defaultProps = {
    isAndroid: Platform.OS === 'android',
  }
  dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  state = {
    isLoading: true,
    refreshing: false,
    dataSource: this.dataSource.cloneWithRows([]),
    showStats: false,
    settings: {},
  }
  _navigate(station) {
    let moreInfoRoute = Routes.moreInfo();
    moreInfoRoute.passProps = { station: station };
    this.props.navigator.push(moreInfoRoute);
  }
  async onRefresh() {
    await this.props.searchAlerts(this.props.alerts.query);
    const formatedOriginalTime = dateFormat(new Date(), 'HH:MM:ss - dd/mm/yyyy');
    if (this.props.isAndroid) {
      ToastAndroid.showWithGravity(
        `Alarmi u ${formatedOriginalTime}`,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );
    }
  }
  _search(query = '') {
    clearTimeout(this.timeoutID);
    this.timeoutID = setTimeout(() => this.props.searchAlerts(query.toUpperCase()), 250);
    this.setState({isLoading: true});
  }
  _clearText() {
    dismissKeyboard();
    clearTimeout(this.timeoutID);
    this.setState({
      isLoading: true,
      refreshing: false,
    });
    this.props.searchAlerts();
  }
  _renderSeparator = (sectionID, rowID, adjacentRowHighlighted) => (
    <View
      key={`${sectionID}${rowID}`}
      style={[{padding: 3}, SharedStyles.rowSeparator, adjacentRowHighlighted && SharedStyles.rowSeparatorHighlighted]}
    />
  )
  _renderRow = (alert, sectionID, rowID, highlightRowFunction) => (
    <AlertCell
      onPress={() => this._navigate(alert)}
      alert={alert}
      onHighlight={() => highlightRowFunction(sectionID,rowID)}
      onDeHighlight={() => highlightRowFunction(null,null)}
    />
  )
  _getSettings = async () => {
    const settings = await Storage.get(Const.ALERTS_SETTINGS);
    if (settings) {
      this.setState({settings: JSON.parse(settings)});
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.alerts[nextProps.alerts.query] && this.timeoutID) {
      this.setState({
        isLoading: false,
        refreshing: false,
        showStats: true,
        dataSource: this.dataSource.cloneWithRows(nextProps.alerts[nextProps.alerts.query])
      });
    }
    this._getSettings();
  }
  componentWillUpdate() {
    LayoutAnimation.configureNext(CustomLayoutAnimation);
    // LayoutAnimation.linear();
  }
  componentWillMount() {
    this._search();
    this._getSettings();
  }
  componentWillUnmount() {
    clearTimeout(this.timeoutID);
  }
  render() {
    let content = null;
    if (this.state.isLoading) {
      content = <View style={SharedStyles.emptyList}>
        <ActivityIndicator 
          animating={true} 
          size='large' 
          color={StyleVars.Colors.primary}
        />
      </View>;
    } 
    else if (this.state.dataSource.getRowCount() === 0) {
      content = <ScrollView keyboardDismissMode='interactive'>
        <View 
          style={SharedStyles.emptyList}
        >
          <Text 
            style={SharedStyles.emptyListText}
          >
            {Const.THERES_NO_SUCH_ALERT}
          </Text>
        </View>
      </ScrollView>
    }
    else {
      content = <ListView
        refreshControl={<RefreshControl
          colors={[StyleVars.Colors.primary]}
          tintColor ={StyleVars.Colors.primary}
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh.bind(this)}/>}
        initialListSize={5}
        pageSize={10}
        enableEmptySections={true}
        dataSource={this.state.dataSource}
        renderSeparator={this._renderSeparator}
        renderRow={this._renderRow.bind(this)}
        automaticallyAdjustContentInsets={false}
        keyboardDismissMode='on-drag'/>;
    }
    const search = this.props.isSearchVisible ? <SearchBar
      settings={this.state.settings}
      placeholder={Const.ALERTS_SEARCH_PLACEHOLDER}
      isLoading={this.state.isLoading}
      clearText={this._clearText.bind(this)}
      onSearch={e => this._search(e.nativeEvent.text.toUpperCase())}
    /> : null;
    return (
      <View 
        style={SharedStyles.container}
      >
        { search }
        { content }
        { this.state.showStats ? <StatsView 
          severity={true} 
          data={Stats.create(this.props.alerts[this.props.alerts.query])}/> : null }
      </View>
   	);
  }
}

function mapState({alerts}) {
  return { alerts };
}

function mapDispatch(dispatch) {
  return bindActionCreators({ searchAlerts }, dispatch);
}

export default connect(mapState, mapDispatch)(Interval(Alerts));