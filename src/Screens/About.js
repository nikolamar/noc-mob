import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  ActivityIndicator,
  Text,
  View
} from 'react-native';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import StyleVars from 'NOC/src/styles/StyleVars';
import Const from 'NOC/src/lib/Const';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: StyleVars.Colors.primary
  },
  logo: {
    width: Const.CIRCLE_RADIUS,
    height: Const.CIRCLE_RADIUS,
    marginBottom: 200
  },
  logoTextContaner: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoText: {
    color: 'white',
    fontSize: 17,
    fontWeight: '600',
    fontFamily: StyleVars.Fonts.general,
    marginBottom: 200
  },
  loader: {
    position: 'absolute',
    bottom: 50,
    left: 0,
    right: 0
  },
  author: {
    position: 'absolute',
    color: 'white',
    left: 0,
    right: 0,
    bottom: 100,
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '400',
    fontFamily: StyleVars.Fonts.general
  }
});

const About = () => (
  <View 
    style={styles.container}
  >
    <Image 
      source={require('NOC/img/logo.png')} 
      style={styles.logo} 
    />
    <View 
      style={styles.logoTextContaner}
    >
      <Text 
        style={styles.logoText}>
        {Const.APP_TITLE}
      </Text>
    </View>
    <Text 
      style={styles.author}>
      {Const.AUTHOR}
    </Text>
  </View>
);

export default About;