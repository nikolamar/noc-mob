import React, { Component, PropTypes } from 'react';
import {
  Alert,
  Animated,
  TouchableHighlight,
  ActivityIndicator,
	View,
	Text,
  TouchableWithoutFeedback
} from 'react-native';
import Button from 'NOC/src/Views/Button';
import Storage from 'NOC/src/lib/Storage';
import Const from 'NOC/src/lib/Const';
import StyleVars from 'NOC/src/styles/StyleVars';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import Routes from 'NOC/src/Routes';
import MapView from 'react-native-maps';
import SearchBar from 'NOC/src/Views/SearchBar';
import { connect } from 'react-redux';
import { searchMarkers } from 'NOC/src/actions';
import { bindActionCreators } from 'redux';
import SiteMarker from 'NOC/src/Views/SiteMarker';
import dismissKeyboard from 'dismissKeyboard';

Object.size = obj => {
  let size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

class Map extends Component {
  state = {
    isLoading: false,
    refreshing: false,
    initialRegion: {
      latitude: 44.8021,
      longitude: 20.4664,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
    markers: {},
    markersLength: 0,
    size: Const.LARGE,
    currentMarker: {},
    settings: {},
  }
  _search(query = '') {
    this._cancelRequest();
    if (query.length == 0 || query.length >= 3) {
      this.setState({
        isLoading: true,
        refreshing: false,
      });
      this.timeoutID = setTimeout(() => this.props.searchMarkers(query), 250);
      this.setState({isLoading: true});
    }
  }
  _cancelRequest() {
    clearTimeout(this.timeoutID);
    this.timeoutID = null;
  }
  _clearText() {
    dismissKeyboard();
    this._search();
    this.setState({
      isLoading: false,
      refreshing: false,
    });
  }
  _onMarkerPress(marker) {
    this._navigate(marker.stations);
    this._cancelRequest();
    this.setState({
      isLoading: false,
      refreshing: false,
      currentMarker: marker
    });
  }
  _navigate(stations) {
    let moreInfoRoute = Routes.moreInfo();
    moreInfoRoute.passProps = { stations: stations };
    this.props.navigator.push(moreInfoRoute);
  }
  _animateToMarker(marker) {
    dismissKeyboard();
    if (marker.lat === undefined || marker.lat === 0) {
      Alert.alert(
        Const.ERROR_TITLE,
        Const.ERROR_MAP_POSITION,
      );
      return;
    }
    this.map.animateToRegion({
      latitude: marker.lat,
      longitude: marker.lon,
      latitudeDelta: this.state.initialRegion.latitudeDelta,
      longitudeDelta: this.state.initialRegion.longitudeDelta
    }, 1000);
    this.setState({
      currentMarker: marker
    });
  }
  _getSettings = async () => {
    const settings = await Storage.get(Const.MAP_SETTINGS);
    if (settings) {
      this.setState({settings: JSON.parse(settings)});
    }
  }
  async _onPress() {
    this.setState({isLoading: true});
    await this.props.searchFailures();
    this.setState({isLoading: false});
  }
  componentWillReceiveProps(nextProps) {
    this._getSettings();
    if (nextProps.markers.settings) {
      this.setState({size: nextProps.markers.settings.size || Const.LARGE});
    }
    if (nextProps.markers[nextProps.markers.query]) {
      const markers = nextProps.markers[nextProps.markers.query];
      if (nextProps.markers.settings) {
        this.setState({
          size: nextProps.markers.settings.size,
        });
      }
      this.setState({
        isLoading: false,
        refreshing: false,
        markers: markers,
      });
      const marker = markers[Object.keys(markers)[0]];
      if (marker.lon) {
        this._animateToMarker(marker);
      }
    }
  }
  async componentWillMount() {
    this._getSettings();
    await this.props.searchMarkers();
    const settings = this.props.markers.settings ? JSON.parse(this.props.markers.settings) : null;
    const currentMarker = this.props.markers[''][Object.keys(this.props.markers[''])[0]];
    this.setState({
      markers: this.props.markers[''],
      markersLength: Object.size(this.props.markers['']),
      currentMarker: currentMarker,
      initialRegion: {
        latitude: currentMarker.lat,
        longitude: currentMarker.lon,
        latitudeDelta: this.state.initialRegion.latitudeDelta,
        longitudeDelta: this.state.initialRegion.longitudeDelta,
      },
      size: settings ? settings.size : Const.LARGE
    });
  }
  render() {
    let content = null;
    content = Object.keys(this.state.markers).map(key => 
      <SiteMarker
        onPress={() => this._onMarkerPress(this.state.markers[key])}
        key={key}
        size={this.state.size || Const.LARGE} 
        site={this.state.markers[key]}
      />
    );
    const search = this.props.isSearchVisible ? <SearchBar
      settings={this.state.settings}
      placeholder={Const.STATIONS_SEARCH_PLACEHOLDER}
      isLoading={this.state.isLoading}
      clearText={this._clearText.bind(this)}
      onSearch={event => this._search(event.nativeEvent.text.toUpperCase())}
    /> : null;
    return (
      <View 
        style={SharedStyles.container}
      >
        { search }
        <TouchableWithoutFeedback 
          onPress={dismissKeyboard}
        >
          <MapView
            style={{flex: 1}}
            ref={ref => this.map = ref}
            initialRegion={this.state.initialRegion}
            showsCompass={true}
            showsUserLocation={true}
            toolbarEnabled={true}
          >
            { content }
          </MapView>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

function calcDistance(lat1, lon1, lat2, lon2) {
  var radlat1 = Math.PI * lat1/180
  var radlat2 = Math.PI * lat2/180
  var theta = lon1-lon2
  var radtheta = Math.PI * theta/180
  var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  dist = Math.acos(dist)
  dist = dist * 180/Math.PI
  dist = dist * 60 * 1.1515
  return dist.toFixed(2)
}

function mapState({markers}) {
  return { markers };
}

function mapDispatch(dispatch) {
  return bindActionCreators({ searchMarkers }, dispatch);
}

export default connect(mapState, mapDispatch)(Map);