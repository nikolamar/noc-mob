import React, { Component, PropTypes } from 'react';
import {
  Alert,
  ListView,
  ScrollView,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import StyleVars from 'NOC/src/styles/StyleVars';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import StationCell from 'NOC/src/Views/StationCell';
import Const from 'NOC/src/lib/Const';

export default class MoreInfo extends Component {
  static propTypes = {
    station: PropTypes.object,
    stations: PropTypes.array
  }
  render() {
    let content = null;
    if (this.props.station) {
      content = <View>
        <StationCell
          expanded={true}
          station={this.props.station} 
        />
      </View>;
    }
    else if (this.props.stations) {
      content = <View>
        {this.props.stations.map((station, i) => 
          <StationCell
            expanded={true}
            key={i} 
            station={station} 
          /> 
        )}
      </View>;
    }
    else {
      content = <View style={SharedStyles.emptyList}>
        <Text style={SharedStyles.emptyListText}>
          { Const.THERES_NO_SUCH_STATION }.
        </Text>
      </View>;
    }
    return (
      <View 
        style={SharedStyles.container}
      >
        <ScrollView>
          { content }
        </ScrollView>
      </View>
    )
  }
}