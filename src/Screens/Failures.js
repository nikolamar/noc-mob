import React, { Component, PropTypes } from 'react';
import {
  UIManager,
  LayoutAnimation,
  ToastAndroid,
  ScrollView,
	ListView,
	View,
	Text,
  ActivityIndicator,
  RefreshControl,
  Platform
} from 'react-native';
import CustomLayoutAnimation from 'NOC/src/lib/CustomLayoutAnimation';
import Storage from 'NOC/src/lib/Storage';
import StyleVars from 'NOC/src/styles/StyleVars';
import dateFormat from 'dateformat';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import Routes from 'NOC/src/Routes';
import SearchBar from 'NOC/src/Views/SearchBar';
import FailureCell from 'NOC/src/Views/FailureCell';
import Stats from 'NOC/src/lib/Stats';
import StatsView from 'NOC/src/Views/StatsView';
import Const from 'NOC/src/lib/Const';
import Interval from 'NOC/src/lib/Interval';
import { connect } from 'react-redux';
import { searchFailures } from 'NOC/src/actions/index';
import { bindActionCreators } from 'redux';
import dismissKeyboard from 'dismissKeyboard';

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Failures extends Component {
  static propTypes = {
    failures: PropTypes.object.isRequired,
    isAndroid: PropTypes.bool,
  }
  static defaultProps = {
    isAndroid: Platform.OS === 'android',
  }
  dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
  state = {
    isLoading: true,
    refreshing: false,
    dataSource: this.dataSource.cloneWithRows([]),
    showStats: false,
    settings: {},
  };
  _search(query = '') {
    clearTimeout(this.timeoutID);
    if (query.length > 3) {
      this.setState({
        isLoading: true
      });
    }
    this.timeoutID = setTimeout(() => this.props.searchFailures(query), 250);
  }
  _clearText() {
    dismissKeyboard();
    clearTimeout(this.timeoutID);
    this.setState({
      isLoading: true,
      refreshing: false,
    });
    this.props.searchFailures();
  }
  async _onRefresh() {
    await this.props.searchFailures(this.props.failures.query);
    const formatedOriginalTime = dateFormat(new Date(), 'HH:MM:ss - dd/mm/yyyy');
    if (this.props.isAndroid) {
      ToastAndroid.showWithGravity(
        `Smetnje u ${formatedOriginalTime}`,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
      );
    }
  }
  _renderSeparator = (sectionID, rowID, adjacentRowHighlighted) => (
    <View
      key={`${sectionID}${rowID}`}
      style={[{padding: 3}, SharedStyles.rowSeparator, adjacentRowHighlighted && SharedStyles.rowSeparatorHighlighted]}
    />
  )
  _renderRow = (failure, sectionID, rowID, highlightRowFunction) => (
    <FailureCell
      navigator={this.props.navigator}
      failure={failure}
      onHighlight={() => highlightRowFunction(sectionID,rowID)}
      onDeHighlight={() => highlightRowFunction(null,null)}
    />
  )
  _getSettings = async () => {
    const settings = await Storage.get(Const.FAILURES_SETTINGS);
    if (settings) {
      this.setState({settings: JSON.parse(settings)});
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.failures[nextProps.failures.query] && this.timeoutID) {
      this.setState({
        isLoading: false,
        refreshing: false,
        showStats: true,
        dataSource: this.dataSource.cloneWithRows(nextProps.failures[nextProps.failures.query])
      });
    }
    this._getSettings();
  }
  componentWillUpdate() {
    LayoutAnimation.configureNext(CustomLayoutAnimation);
    // LayoutAnimation.linear();
  }
  componentWillMount() {
    this._search();
    this._getSettings();
  }
  componentWillUnmount() {
    clearTimeout(this.timeoutID);
  }
  render() {
    let content = null;
    if (this.state.isLoading) {
      content = <View style={SharedStyles.emptyList}>
        <ActivityIndicator 
          animating={true} 
          size='large' 
          color={StyleVars.Colors.primary}
        />
      </View>;
    }
    else if (this.state.dataSource.getRowCount() === 0) {
      content = <ScrollView keyboardDismissMode='interactive'>
        <View 
          style={SharedStyles.emptyList}
        >
          <Text 
            style={SharedStyles.emptyListText}>
            {Const.THERES_NO_SUCH_FAILURE}.
          </Text>
        </View>
      </ScrollView>
    }
    else {
      content = <ListView
        enableEmptySections={false}
        refreshControl={<RefreshControl
          colors={[StyleVars.Colors.primary]}
          tintColor ={StyleVars.Colors.primary}
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}/>}
        initialListSize={5}
        pageSize={10}
        enableEmptySections={true}
        dataSource={this.state.dataSource}
        renderSeparator={this._renderSeparator}
        renderRow={this._renderRow.bind(this)}
        automaticallyAdjustContentInsets={false}
        keyboardDismissMode='on-drag'
      />;
    }
    const search = this.props.isSearchVisible ? <SearchBar
      settings={this.state.settings}
      placeholder={Const.FAILURES_SEARCH_PLACEHOLDER}
      isLoading={this.state.isLoading}
      clearText={this._clearText.bind(this)}
      onSearch={e => this._search(e.nativeEvent.text.toUpperCase())}
    /> : null;
    return (
      <View 
        style={SharedStyles.container}
      >
        { search }
        { content }
        { this.state.showStats ? <StatsView 
          tech={true} 
          priority={true}
          data={Stats.create(this.props.failures[this.props.failures.query])}/> : null }
      </View>
   	);
  }
}

function mapState({failures}) {
  return { failures }
}

function mapDispatch(dispatch) {
  return bindActionCreators({ searchFailures }, dispatch);
}

export default connect(mapState, mapDispatch)(Interval(Failures));