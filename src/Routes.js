import Failures from 'NOC/src/Screens/Failures';
import FailuresSettings from 'NOC/src/Screens/FailuresSettings';
import Stations from 'NOC/src/Screens/Stations';
import StationsSettings from 'NOC/src/Screens/StationsSettings';
import Alerts from 'NOC/src/Screens/Alerts';
import AlertsSettings from 'NOC/src/Screens/AlertsSettings';
import Map from 'NOC/src/Screens/Map';
import MapSettings from 'NOC/src/Screens/MapSettings';
import MoreInfo from 'NOC/src/Screens/MoreInfo';
import About from 'NOC/src/Screens/About';
import Const from 'NOC/src/lib/Const';
import { MenuButton, BackButton, SearchAndMoreButtons } from 'NOC/src/Views/Button';

class Routes {
  failures = (props={}, type='Normal') => ({
    name: 'Failures',
    title: Const.ROUTE_FAILURES_TITLE,
    component: Failures,
    leftButton: MenuButton,
    rightButton: SearchAndMoreButtons,
    rightButtonRoute: this.failuresSettings,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_NORMAL
  })
  failuresSettings = (props={}, type='Normal') => ({
    name: 'FailuresSettings',
    title: Const.ROUTE_SETTINGS_TITLE,
    component: FailuresSettings,
    leftButton: BackButton,
    rightButton: null,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_SETTINGS
  })
  stations = (props={}, type='Normal') => ({
    name: 'Stations',
    title: Const.ROUTE_STATIONS_TITLE,
    component: Stations,
    leftButton: MenuButton,
    rightButton: SearchAndMoreButtons,
    rightButtonRoute: this.stationsSettings,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_NORMAL
  })
  stationsSettings = (props={}, type='Modal') => ({
    name: 'StationsSettings',
    title: Const.ROUTE_SETTINGS_TITLE,
    component: StationsSettings,
    leftButton: BackButton,
    rightButton: null,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_SETTINGS
  })
  alerts = (props={}, type='Normal') => ({
    name: 'Alerts',
    title: Const.ROUTE_ALERTS_TITLE,
    component: Alerts,
    leftButton: MenuButton,
    rightButton: SearchAndMoreButtons,
    rightButtonRoute: this.alertsSettings,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_NORMAL
  })
  alertsSettings = (props={}, type='Normal') => ({
    name: 'AlertsSettings',
    title: Const.ROUTE_SETTINGS_TITLE,
    component: AlertsSettings,
    leftButton: BackButton,
    rightButton: null,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_SETTINGS
  })
  map = (props={}, type='Normal') => ({
    name: 'Map',
    title: Const.ROUTE_MAP_TITLE,
    component: Map,
    leftButton: MenuButton,
    rightButton: SearchAndMoreButtons,
    rightButtonRoute: this.mapSettings,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_NORMAL
  })
  mapSettings = (props={}, type='Normal') => ({
    name: 'AlertsSettings',
    title: Const.ROUTE_SETTINGS_TITLE,
    component: MapSettings,
    leftButton: BackButton,
    rightButton: null,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_SETTINGS
  })
  moreInfo = (props={}, type='Normal') => ({
    name: 'MoreInfo',
    title: Const.ROUTE_MORE_INFO_TITLE,
    component: MoreInfo,
    leftButton: BackButton,
    rightButton: null,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_INFO
  })
  about = (props={}, type='Normal') => ({
    name: 'About',
    title: Const.ROUTE_ABOUT_TITLE,
    component: About,
    leftButton: BackButton,
    rightButton: null,
    passProps: {
      props: props
    },
    type: Const.ROUTE_TYPE_SETTINGS
  })
  login = (props={}, type='Normal') => ({
    name: 'Login',
    title: Const.ROUTE_LOGIN_TITLE
  })
}

export default new Routes();
