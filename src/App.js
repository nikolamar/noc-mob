import React, { Component, PropTypes } from 'react';
import {
  Alert,
  Dimensions,
  AppRegistry,
  StyleSheet,
  View
} from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import Login from 'NOC/src/Screens/Login';
import Splash from 'NOC/src/Screens/Splash';
import RootNavigator from 'NOC/src/RootNavigator';
import Const from 'NOC/src/lib/Const';
import Storage from 'NOC/src/lib/Storage';
import createLogger from 'redux-logger';
import reducers from 'NOC/src/reducers';

const store = createStore(
  reducers,
  applyMiddleware(thunk)
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0
  }
});

class App extends Component {
  state = {
    isAppLoading: true,
    authenticated: null,
    user: '',
    pass: '',
    token: '',
    privilege: null
  }
  async componentWillMount() {
    const authenticated = await Storage.get(Const.AUTHENTICATED);
    this.setState({
      user: await Storage.get(Const.USERNAME) || '',
      authenticated: authenticated,
      isAppLoading: false
    });
  }
  async _authenticate(credentials) {
    await this._saveUser(credentials);
    await this._createDefaults();
    this.setState({
      authenticated: true,
      user: credentials.user,
      pass: credentials.pass,
      token: credentials.token,
      privilege: credentials.privilege
    });
    Alert.alert(
      Const.SUCCESS_TITLE,
      Const.LOGIN_WELCOME_MESSAGE
    );
  }
  async _saveUser(credentials) {
    await Storage.set(Const.AUTHENTICATED, Const.AUTHENTICATED);
    await Storage.set(Const.USERNAME, credentials.user);
    await Storage.set(Const.PASS, credentials.pass);
    await Storage.set(Const.TOKEN, credentials.token);
    await Storage.set(Const.PRIVILEGE, JSON.stringify(credentials.privilege));
  }
  async _createDefaults() {
    let failuresSettings = await Storage.get(Const.FAILURES_SETTINGS);
    let alertsSettings = await Storage.get(Const.ALERTS_SETTINGS);
    let stationsSettings = await Storage.get(Const.STATIONS_SETTINGS);
    if (!failuresSettings) {
      await Storage.set(Const.FAILURES_SETTINGS, JSON.stringify({
        searchProperty: Const.OPENED,
        sortProperty: Const.ORIGINAL_TIME
      }));
    }
    if (!alertsSettings) {
      await Storage.set(Const.ALERTS_SETTINGS, JSON.stringify({
        searchProperty: Const.CODE,
        sortProperty: Const.ORIGINAL_TIME
      }));
    }
    if (!stationsSettings) {
      await Storage.set(Const.STATIONS_SETTINGS, JSON.stringify({
        searchingStorage: false,
        searchProperty: Const.CODE,
        sortProperty: Const.CODE
      }));
    }
  }
  async _logout() {
    await Storage.remove(Const.AUTHENTICATED);
    await Storage.remove(Const.PASS);
    await Storage.remove(Const.TOKEN);
    await Storage.remove(Const.PRIVILEGE);
    this.setState({
      authenticated: false,
      user: await Storage.get(Const.USERNAME) || '',
      pass: '',
      token: '',
      privilege: null
    });
  }
  render() {
    let content = null;
    if (this.state.isAppLoading) {
      content = <Splash />;
    }
    if (!this.state.isAppLoading && this.state.authenticated) {
      content = <RootNavigator logout={this._logout.bind(this)} />;
    }
    if (!this.state.isAppLoading && !this.state.authenticated) {
      content = <Login authenticate={this._authenticate.bind(this)} />;
    }
    return (
      <Provider store={store}>
        { content }
      </Provider>
    );
  }
}

export default App;