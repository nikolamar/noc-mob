export default {
  Colors: {
    primary: '#f44336',
    primaryText: '#212121',
    secondaryText: '#263238',
    titleText: 'white',
    navBarBackground: '#f44336',
    lightBackground: '#FFEBEE',
    mediumBackground: '#FFEBEE',
    darkBackground: '#D50000',
    veryDarkBackground: '#B71C1C',
    contentBackground: 'white',
    darkContentBackground: 'black',
  },
  Fonts: {
    logo: "Roboto",
    general: "Roboto"
  }
}
