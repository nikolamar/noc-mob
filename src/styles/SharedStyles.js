import { StyleSheet } from 'react-native';
import StyleVars from 'NOC/src/styles/StyleVars';

export default StyleSheet.create({
  navigator: {
    borderBottomWidth: 1,
    borderBottomColor: StyleVars.Colors.darkBackground,
    backgroundColor: StyleVars.Colors.primary,
    height: 70,
  },
  navigatorIOS: {
    borderBottomWidth: 1,
    borderBottomColor: StyleVars.Colors.darkBackground,
    backgroundColor: StyleVars.Colors.primary,
    height: 80,
  },
  navigatorTitle: {
    color: StyleVars.Colors.titleText,
    fontFamily: StyleVars.Fonts.heading,
    fontWeight: '600',
    fontSize: 20,
    marginVertical: 7,
  },
  navigatorTitleIOS: {
    color: StyleVars.Colors.titleText,
    fontFamily: StyleVars.Fonts.heading,
    fontWeight: '600',
    fontSize: 20,
    marginVertical: 20,
  },
  screenContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    paddingTop: 70,
  },
  screenContainerIOS: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    paddingTop: 80,
  },
  button: {
    marginVertical: 20, 
    marginHorizontal: 10
  },
  buttonIOS: {
    marginVertical: 15, 
    marginHorizontal: 10
  },
  loader: {
    height: 30
  },
  container: {
    flex: 1,
    backgroundColor: StyleVars.Colors.contentBackground,
  },
  overlay: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  headingText: {
    color: StyleVars.Colors.primaryText,
    fontFamily: StyleVars.Fonts.heading,
    fontSize: 12,
    fontWeight: '600'
  },
  rowSeparator: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    height: 1
  },
  rowSeparatorHighlighted: {
    opacity: 0.0
  },
  searchContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: StyleVars.Colors.darkBackground
  },
  searchInput: {
    flex: 1,
    marginVertical: 10,
    fontSize: 15,
    height: 40,
    color: 'white'
  },
  searchSpinner: {
    height: 50,
    width: 30
  },
  emptyList: {
    paddingTop: 100,
    flex: 1,
    alignItems: 'center',
  },
  emptyListText: {
    color: StyleVars.Colors.primaryText,
    fontFamily: StyleVars.Fonts.general,
    fontSize: 12,
    fontWeight: '400'
  },
  block: {
    width: 5
  },
  icon: {
    height: 35,
    width: 35,
    resizeMode: 'contain',
  },
  cellRow: {
    flexDirection: 'row'
  },
  labelText: {
    flex: 1,
    color: StyleVars.Colors.secondaryText,
    fontSize: 9,
    fontWeight: '500',
    marginBottom: 4
  },
  text: {
    flex: 3,
    color: StyleVars.Colors.primaryText,
    textAlign: 'right',
    fontSize: 9,
    fontWeight: '600'
  },
});
