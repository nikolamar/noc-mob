import React, { Component, PropTypes } from 'react';
import {
  BackAndroid,
  Alert,
  Navigator,
  DrawerLayoutAndroid,
  Text,
  View,
  Platform
} from 'react-native';
import SharedStyles from 'NOC/src/styles/SharedStyles';
import Routes from 'NOC/src/Routes';
import AppMenu from 'NOC/src/Views/AppMenu';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SideMenu from 'react-native-side-menu';
import Const from 'NOC/src/lib/Const';
import Storage from 'NOC/src/lib/Storage';

// backgroundColor: StyleVars.Colors.navBarBackground,

export default class RootNavigator extends Component {
  static childContextTypes = {
    navigator: PropTypes.object
  }
  static propTypes = {
    isAndroid: PropTypes.bool,
  }
  static defaultProps = {
    isAndroid: Platform.OS === 'android',
  }
  state = {
    isMenuOpen: false,
    isSearchVisible: false,
  }
  componentWillMount() {
    const { isAndroid } = this.props;
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (this.navigator) {
        const routesList = this.navigator.getCurrentRoutes();
        if (routesList.length > 1) {
          this.navigator.pop();
          return true;
        }
      }
      Alert.alert(
        Const.NOTIFICATION,
        Const.EXIT_MESSAGE,
        [
          {text: Const.CANCEL, onPress: () => {}},
          {text: Const.EXIT, onPress: () => BackAndroid.exitApp()}
        ]
      );
      return true;
    });
    this.routeMapper = {
      LeftButton: (route, navigator, index, navState) => {
        if (!route.leftButton) return;
        return (
          <route.leftButton
            onPress={e => {
              if (e === 'MENU') {
                this.props.isAndroid ? this.drawer.openDrawer() : this.setState({isMenuOpen: true});
              }
              else if (e === 'BACK') {
                this.navigator.pop();
              }
            }}
          />
        );
      },
      RightButton: (route, navigator, index, navState) => {
        if (!route.rightButton) return;
        return (
          <route.rightButton
            onPress={e => {
              if (e === 'MORE') {
                this._navigate(route.rightButtonRoute());
              }
              else if (e === 'SEARCH') {
                this.setState({isSearchVisible: !this.state.isSearchVisible});
              }
            }}
          />
        );
      },
      Title: (route, navigator, index, navState) => (
        <Text 
          style={
            this.props.isAndroid ?
            SharedStyles.navigatorTitle :
            SharedStyles.navigatorTitleIOS
          }
          numberOfLines={1}>
          {route.title}
        </Text>
      )
    };
    this._firstCheck();
  }
  async _firstCheck() {
    const isUpdateConfirmed = await Storage.get(Const.UPDATE_CONFIRM);
    if (isUpdateConfirmed) {
      return;
    }
    Alert.alert(
      Const.UPDATE_STATIONS_TITLE,
      Const.UPDATE_STATIONS_MESSAGE,
      [
        {text: Const.DONT_ASK_AGAIN, onPress: this._dontAskAgain.bind(this)},
        {text: Const.CANCEL, onPress: () => {}},
        {text: Const.OK, onPress: this._confirm.bind(this)},
      ]
    );
  }
  _dontAskAgain() {
    Storage.set(Const.UPDATE_CONFIRM, Const.UPDATE_CONFIRM);
  }
  _confirm() {
    Storage.set(Const.UPDATE_CONFIRM, Const.UPDATE_CONFIRM);
    this._navigate(Routes.stations());
    this._navigate(Routes.stationsSettings());
  }
  _configureScene = (route, routeStack) => ({
    ...Navigator.SceneConfigs.PushFromRight,
    gestures: {},
  });
  _navigate(route) {
    const routesList = this.navigator.getCurrentRoutes();
    this.props.isAndroid ? this.drawer.closeDrawer() : this.setState({isMenuOpen: false});
    if (route.name === 'Login') {
      this.props.logout();
      return;
    }
    for (let i in routesList) {
      if (routesList[i].name === route.name) {
        this.navigator.jumpTo(routesList[i]);
        return;
      }
    }
    this.navigator.push(route);
  }
  _renderScene = (route, navigator) => (
    <View 
      style={this.props.isAndroid ? 
        SharedStyles.screenContainer :
        SharedStyles.screenContainerIOS
      }
    >
      <route.component
        {...route.passProps}
        isSearchVisible={this.state.isSearchVisible}
        navigator={navigator}
      />
    </View>
  )
  render() {
    const { isAndroid } = this.props;
    const navigator = <Navigator
      ref={ref => this.navigator = ref}
      initialRoute={Routes.failures()}
      renderScene={this._renderScene}
      configureScene={this._configureScene}
      navigationBar={
        <Navigator.NavigationBar 
          style={this.props.isAndroid ? 
            SharedStyles.navigator : 
            SharedStyles.navigatorIOS
          } 
          routeMapper={this.routeMapper}
        />
      }
    />;
    if (isAndroid) {
      return (
        <DrawerLayoutAndroid
          ref={ref => this.drawer = ref}
          drawerWidth={Const.NAVIGATOR_WIDTH}
          drawerPosition={DrawerLayoutAndroid.positions.Left}
          renderNavigationView={() => 
            <AppMenu 
              onPress={this._navigate.bind(this)}
            />
          }>
          { navigator }
        </DrawerLayoutAndroid>
      );
    } 
    return (
      <SideMenu
        isOpen={this.state.isMenuOpen}
        openMenuOffset={Const.NAVIGATOR_WIDTH}
        menu={
          <AppMenu 
            onPress={this._navigate.bind(this)}
          />
        }>
        { navigator }
      </SideMenu>
    );
  }
}