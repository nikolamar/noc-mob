import Const from 'NOC/src/lib/Const';

export default (state = {}, action) => {
	switch(action.type) {
		case Const.REDUX_ALERTS:
			return action.payload;

		case Const.REDUX_SORT_ALERTS:
			return action.payload;
			
		default:
			return state;
	}
};
