import Const from 'NOC/src/lib/Const';

export default (state = {}, action) => {
	switch(action.type) {
		case Const.REDUX_SEARCH_STATIONS:
			return {...state, ...action.payload};
			
		case Const.REDUX_SORT_STATIONS:
			return {...state, ...action.payload};

		default:
			return state;
	}
};
