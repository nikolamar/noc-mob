import { combineReducers } from 'redux';
import failuresReducer from 'NOC/src/reducers/failures-reducer';
import alertsReducer from 'NOC/src/reducers/alerts-reducer';
import stationsReducer from 'NOC/src/reducers/stations-reducer';
import markersReducer from 'NOC/src/reducers/markers-reducer';

export default combineReducers({
	failures: failuresReducer,
	alerts: alertsReducer,
	stations: stationsReducer,
	markers: markersReducer
});