import Const from 'NOC/src/lib/Const';

export default (state = {}, action) => {
	switch(action.type) {
		case Const.REDUX_SEARCH_MARKERS:
			return {...state, ...action.payload};

		case Const.REDUX_UPDATE_MARKERS:
			return {...state, ...action.payload};		
		
		default:
			return state;
	}
}