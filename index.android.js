/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import codePush from 'react-native-code-push';
import App from 'NOC/src/App.js';
import Const from 'NOC/src/lib/Const';

const codePushOptions = {
	updateDialog: {
		title: Const.UPDATE_AVAILABLE_TITLE,
		mandatoryUpdateMessage: Const.MANDATORY_UPDATE_MESSAGE,
		optionalUpdateMessage: Const.OPTIONAL_UPDATE_MESSAGE,
		mandatoryContinueButtonLabel: Const.CONTINUE,
		optionalIgnoreButtonLabel: Const.IGNORE,
		optionalInstallButtonLabel: Const.INSTALL
	},
	installMode: codePush.InstallMode.IMMEDIATE,
	checkFrequency: codePush.CheckFrequency.ON_APP_RESUME 
};

export default class NOC extends Component {
	render() {
		return <App />;
	}
}

NOC = codePush(codePushOptions)(NOC);

AppRegistry.registerComponent('NOC', () => NOC);
